<?
session_start();
include "admin/config.php";
include "mysql.class.php"; 
include "settings.class.php"; 
include "template.class.php";
include "parser.class.php";
include "user.class.php";
include "menu.class.php";


$template =  new template;
$mysql    =  new mysql;
$settings =  new settings;
$parser   =  new parser;
$user     =  new join;
$menus     =  new menu;

$mysql -> connection(); 

$setting  =  $settings -> get_settings();

define("theme",$setting["site_theme"]);

if(!empty($_POST['login'])) {

    $user -> login($_POST['login'],$_POST['password']);

}

function display_error($type,$error) { 

print "
<script type='text/javascript'> 

alert(".$error.");

</script>";

} 

function redirect($where) {
    
  print '<script type="text/javascript"> window.location = "'.$where.'"</script>';
  
}

?>
