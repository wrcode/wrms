<?

   
    class menu {
	
	
	
	    private $db;
	 
	    public function __construct() 
        {
            $this->db = new mysql;
			$this->db->connection();
	    }
		
		private $a = array( 'Ę', 'Ó', 'Ą', 'Ś', 'Ł', 'Ż', 'Ź', 'Ć', 'Ń', 'ę', 'ó', 'ą', 
				'ś', 'ł', 'ż', 'ź', 'ć', 'ń' );
				
		private $b = array( 'E', 'O', 'A', 'S', 'L', 'Z', 'Z', 'C', 'N', 'e', 'o', 'a', 
					's', 'l', 'z', 'z', 'c', 'n' );
		
		public function Parse( $string )
		{
			$string = str_replace( $this -> a, $this -> b, $string );
			$string = preg_replace( '#[^a-z0-9]#is', ' ', $string );
			$string = trim( $string );
			$string = preg_replace( '#\s{2,}#', ' ', $string );
			$string = str_replace( ' ', '_', $string );
			return $string;
		}
		
		private function url_slug($str, $options = array()) {
			// Make sure string is in UTF-8 and strip invalid UTF-8 characters
			$str = mb_convert_encoding((string)$str, 'UTF-8', mb_list_encodings());
			
			$defaults = array(
				'delimiter' => '_',
				'limit' => null,
				'lowercase' => true,
				'replacements' => array(),
				'transliterate' => false,
			);
			
			// Merge options
			$options = array_merge($defaults, $options);
			
			$char_map = array(
				// Latin
				'A' => 'A', 'Á' => 'A', 'Â' => 'A', 'A' => 'A', 'Ä' => 'A', 'A' => 'A', 'A' => 'AE', 'Ç' => 'C', 
				'E' => 'E', 'É' => 'E', 'E' => 'E', 'Ë' => 'E', 'I' => 'I', 'Í' => 'I', 'Î' => 'I', 'I' => 'I', 
				'?' => 'D', 'N' => 'N', 'O' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'O' => 'O', 'Ö' => 'O', 'Ő' => 'O', 
				'O' => 'O', 'U' => 'U', 'Ú' => 'U', 'U' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', '?' => 'TH', 
				'ß' => 'ss', 
				'a' => 'a', 'á' => 'a', 'â' => 'a', 'a' => 'a', 'ä' => 'a', 'a' => 'a', 'a' => 'ae', 'ç' => 'c', 
				'e' => 'e', 'é' => 'e', 'e' => 'e', 'ë' => 'e', 'i' => 'i', 'í' => 'i', 'î' => 'i', 'i' => 'i', 
				'?' => 'd', 'n' => 'n', 'o' => 'o', 'ó' => 'o', 'ô' => 'o', 'o' => 'o', 'ö' => 'o', 'ő' => 'o', 
				'o' => 'o', 'u' => 'u', 'ú' => 'u', 'u' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', '?' => 'th', 
				'y' => 'y',

				// Latin symbols
				'©' => '(c)',

				// Greek
				'?' => 'A', '?' => 'B', '?' => 'G', '?' => 'D', '?' => 'E', '?' => 'Z', '?' => 'H', '?' => '8',
				'?' => 'I', '?' => 'K', '?' => 'L', '?' => 'M', '?' => 'N', '?' => '3', '?' => 'O', '?' => 'P',
				'?' => 'R', '?' => 'S', '?' => 'T', '?' => 'Y', '?' => 'F', '?' => 'X', '?' => 'PS', '?' => 'W',
				'?' => 'A', '?' => 'E', '?' => 'I', '?' => 'O', '?' => 'Y', '?' => 'H', '?' => 'W', '?' => 'I',
				'?' => 'Y',
				'?' => 'a', 'ß' => 'b', '?' => 'g', '?' => 'd', '?' => 'e', '?' => 'z', '?' => 'h', '?' => '8',
				'?' => 'i', '?' => 'k', '?' => 'l', 'µ' => 'm', '?' => 'n', '?' => '3', '?' => 'o', '?' => 'p',
				'?' => 'r', '?' => 's', '?' => 't', '?' => 'y', '?' => 'f', '?' => 'x', '?' => 'ps', '?' => 'w',
				'?' => 'a', '?' => 'e', '?' => 'i', '?' => 'o', '?' => 'y', '?' => 'h', '?' => 'w', '?' => 's',
				'?' => 'i', '?' => 'y', '?' => 'y', '?' => 'i',

				// Turkish
				'Ş' => 'S', 'I' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'G' => 'G',
				'ş' => 's', 'i' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'g' => 'g', 

				// Russian
				'?' => 'A', '?' => 'B', '?' => 'V', '?' => 'G', '?' => 'D', '?' => 'E', '?' => 'Yo', '?' => 'Zh',
				'?' => 'Z', '?' => 'I', '?' => 'J', '?' => 'K', '?' => 'L', '?' => 'M', '?' => 'N', '?' => 'O',
				'?' => 'P', '?' => 'R', '?' => 'S', '?' => 'T', '?' => 'U', '?' => 'F', '?' => 'H', '?' => 'C',
				'?' => 'Ch', '?' => 'Sh', '?' => 'Sh', '?' => '', '?' => 'Y', '?' => '', '?' => 'E', '?' => 'Yu',
				'?' => 'Ya',
				'?' => 'a', '?' => 'b', '?' => 'v', '?' => 'g', '?' => 'd', '?' => 'e', '?' => 'yo', '?' => 'zh',
				'?' => 'z', '?' => 'i', '?' => 'j', '?' => 'k', '?' => 'l', '?' => 'm', '?' => 'n', '?' => 'o',
				'?' => 'p', '?' => 'r', '?' => 's', '?' => 't', '?' => 'u', '?' => 'f', '?' => 'h', '?' => 'c',
				'?' => 'ch', '?' => 'sh', '?' => 'sh', '?' => '', '?' => 'y', '?' => '', '?' => 'e', '?' => 'yu',
				'?' => 'ya',

				// Ukrainian
				'?' => 'Ye', '?' => 'I', '?' => 'Yi', '?' => 'G',
				'?' => 'ye', '?' => 'i', '?' => 'yi', '?' => 'g',

				// Czech
				'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U', 
				'Ž' => 'Z', 
				'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',
				'ž' => 'z', 

				// Polish
				'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z', 
				'Ż' => 'Z', 
				'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',
				'ż' => 'z',

				// Latvian
				'A' => 'A', 'Č' => 'C', 'E' => 'E', 'G' => 'G', 'I' => 'i', 'K' => 'k', 'L' => 'L', 'N' => 'N', 
				'Š' => 'S', 'U' => 'u', 'Ž' => 'Z',
				'a' => 'a', 'č' => 'c', 'e' => 'e', 'g' => 'g', 'i' => 'i', 'k' => 'k', 'l' => 'l', 'n' => 'n',
				'š' => 's', 'u' => 'u', 'ž' => 'z'
			);
			
			// Make custom replacements
			$str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);
			
			// Transliterate characters to ASCII
			if ($options['transliterate']) {
				$str = str_replace(array_keys($char_map), $char_map, $str);
			}
			
			// Replace non-alphanumeric characters with our delimiter
			$str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);
			
			// Remove duplicate delimiters
			$str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);
			
			// Truncate slug to max. characters
			$str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');
			
			// Remove delimiter from ends
			$str = trim($str, $options['delimiter']);
		    
			$str = $this -> Parse($str);
			
			return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
		}	
		
		
		public function get_menu($menu,$type,$attach,$element) {
		
		    
			
			
			
			$menu = $this -> db -> clean($menu);
        
            $menus = $this -> db -> query("SELECT  *  FROM table_menus where menu_name = '$menu' or menu_id = '$menu' ",'array');
            
            $menu_id = $menus[0] -> menu_id;
            
            $menu_item = $this -> db -> query("SELECT * FROM table_menus_items where item_menu_id = '$menu_id' ORDER BY item_menu_pos ASC ",'array');
			
			if($type == 'ul')  $menu_ready .='<ul '.$attach.'>'; 
			
			if($type == 'nav')  $menu_ready .='<nav '.$attach.'>'; 
			
			for($i=0;$i<count($menu_item);$i++) {
			    
				if( $this->url_slug($menu_item[$i]->item_name) == 'strona_glowna' ) {
					
					$menu_ready .='<li><a href="home">'.$element.' '.$menu_item[$i]->item_name.'</a>';
					
				} else {
					
					    $menu_ready .='<li><a href="'.$this->url_slug($menu_item[$i]->item_name).'">'.$element.' '.$menu_item[$i]->item_name.'</a>';
					
				}
			
			
			
			}
			
			if($type == 'ul')  $menu_ready .='</ul>'; 
			
			if($type == 'nav')  $menu_ready .='</nav>'; 
			
			
			return $menu_ready;
		
		
		}
		
	
	}

	


?>