<? include "./header.php";
/*                                                                                                                                                                                                                  
      ___           ___           ___           ___     
     /\  \         /\  \         /\  \         /\__\    
    _\:\  \       /::\  \       |::\  \       /:/ _/_   
   /\ \:\  \     /:/\:\__\      |:|:\  \     /:/ /\  \  
  _\:\ \:\  \   /:/ /:/  /    __|:|\:\  \   /:/ /::\  \ 
 /\ \:\ \:\__\ /:/_/:/__/___ /::::|_\:\__\ /:/_/:/\:\__\
 \:\ \:\/:/  / \:\/:::::/  / \:\~~\  \/__/ \:\/:/ /:/  /
  \:\ \::/  /   \::/~~/~~~~   \:\  \        \::/ /:/  / 
   \:\/:/  /     \:\~~\        \:\  \        \/_/:/  /  
    \::/  /       \:\__\        \:\__\         /:/  /   
     \/__/         \/__/         \/__/         \/__/    
	 
	 # Copyright 2013 by wrcode
     # Feel free to modify the source
     # Don't sell without author permission	 
						
*/
 ?>

<? $mysql -> admin_check($_SESSION['admin']); ?>

<?if (!empty($_POST['image_name']) && empty($_GET['edit'])) $gallery ->  gallery_image_add($_POST['gallery_id'],$_POST['image_name'],$_FILES['files'],$_POST['image_desc'],$_POST['image_slider']);?>

<? if($_GET['delete']) $gallery -> gallery_image_delete($_GET['delete']); ?>

<? if($_GET['edit'] && $_POST['image_name']) $gallery -> gallery_image_edit_save($_GET['edit'],$_POST['image_name'],$_POST['image_desc'],$_POST['image_filename'],$_POST['gallery_id'],$_POST['image_slider']); ?>

<? if(!empty($_GET['set_thumbnail'])) $gallery -> gallery_set_thumbnail($_GET['set_thumbnail'],$_GET['gallery_id']); ?>
<div id="header">
    <img src="images/icons/gallery.png" />
    <div class="header-content">
        <span class="header-title">Galeria</span><br />
        <span class="header-tagline">Zarzadzanie galeria</span>
    </div>
</div>

<div id="tabs">

   <a href="index.php?site=main">Strona główna</a>
   <a href="index.php?site=wpisy">Wpisy</a>
   <a href="index.php?site=podstrony">Podstrony</a>
   <span>Galeria</span>
   <a href="index.php?site=ustawienia">Ustawienia</a>
   <a href="index.php?site=user">Uzytkownicy</a>

</div>

<div class="container cf">
    <div class="side-left">
	
        <div class="side-left-title">Galeria</div>
        <div class="side-left-content">
            <a href="index.php?site=galeria&action=add">Stworz galerie</a><br />
            <a href="index.php?site=galeria">Lista galeri</a><br />
        </div>
		
		<div class="side-left-title">Obrazy</div>
        <div class="side-left-content">
            <a href="index.php?site=obraz&action=add">Dodaj obraz</a><br />
			<a href="index.php?site=obraz">Lista obrazow</a><br />
        </div>
	
		
    </div>
    <div class="side-right">
        <div class="side-right-title">Lista obrazow</div>
        <div class="side-right-content">
		
		<? if(!isset($_GET['action']) && empty($_GET['edit'])) { ?>
        
		<ul class="gallery">

		<? $images =  $gallery -> gallery_images($_GET['gallery_id']); ?>
				
		<? for($i=0;$i<count($images);$i++) { ?>
		
		
		<li>
				<span><?=$images[$i] -> gal_name;  ?></span>
		<div class="img">
		<a href="./data/uploads/<?=$images[$i] -> image_filename;  ?>" title="<?=$images[$i] -> image_name;  ?>" rel="lightbox"><img src="./data/uploads/<?=$images[$i] -> image_filename;  ?>" width="100" height="100" /></a>
		</div>
        <a class="podpowiedz" title="Usun obraz" href="index.php?site=obraz&delete=<?=$images[$i] -> image_id;  ?>" onclick="if(confirm('Czy napewno chcesz usunac obraz?')) {} else return false;"><img src="images/icons/ic_delete.png"></a>
		<a class="podpowiedz" title="Edytuj obraz" href="index.php?site=obraz&edit=<?=$images[$i] -> image_id;  ?>"><img src="images/icons/ic_edit.png"></a>
		<? if(!empty($_GET['gallery_id'])) { ?>
		
		<a class="podpowiedz" title="Ustaw jako thumbnail" href="index.php?site=obraz&gallery_id=<?=$images[$i] -> gallery_id;  ?>&set_thumbnail=<?=$images[$i] -> image_id;  ?>"><img src="images/icons/ic_grid.png"></a>
		
		<? } ?>
        </li>
		
	    <? } ?>
		
		
		</ul>
		
				
		<? } ?>
                           
							
<? 

	if($_GET['action']=='add' || isset($_GET['edit'] )) 
	{ 
	if(isset($_GET['edit'])) $edit = '&edit='.$_GET['edit'].'';
    $edited = $gallery -> gallery_image_edit($_GET['edit']); 
							
?> 
							
		<form action="index.php?site=obraz<?=$edit; ?>" enctype="multipart/form-data" method="post">

            <table class="pages-create" width="100%" cellpadding="10px">
                <input type="hidden" name="type" value="static">
                <tbody>
				
			
				
                <tr>
				
                    <td>Tytul</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech" value="<?=$edited->image_name; ?>" name="image_name"></td>
					
					<td>Opis</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech" value="<?=$edited->image_desc; ?>" name="image_desc"></td>
					
					<td>Slider</td>
					<td>
                        <select name="image_slider">
						<?  
						
						 if($edited -> image_slider == 0) { ?>
						 
						  
						  <option value="0" selected="selected">Nie</option>
					      <option value="1">Tak</option>
						 
						  <? } else { ?>
						  
						  <option value="1" selected="selected">Tak</option>
						  <option value="0">Nie</option>
						  
						  
						  <? } ?>
						
					
							
                        </select>
					</td>
              
                </tr>
				
				
				  <tr>
				
                    <td>File</td>
					
					<?if(!empty($_GET['edit'])) { ?>
					
					 <td><input type="text"  x-webkit-speech="x-webkit-speech" value="<?=$edited->image_filename; ?>" name="image_filename"></td>
					
					<? } else { ?>
					
                    <td><input type="file"  name="files"><input type="hidden" name="MAX_FILE_SIZE" value="1000000000" /></td>
					
					<? } ?>
					
					<td>Galeria</td>
                    <td>
					
				<select name="gallery_id"> 
				
                <?  $lista_galeri = $gallery -> gallerys(); ?>
				
                <? for($i=0;$i<count($lista_galeri);$i++) { ?>
				
				<? if(!empty($_GET['edit']) && $edited->gallery_id == $lista_galeri[$i]-> gal_id ) { ?>
				
				 <option selected="selected" value="<?=$lista_galeri[$i]-> gal_id ;?>"><?= $lista_galeri[$i]-> gal_name ; ?></option>
				
				<? } else { ?>

                <option value="<?=$lista_galeri[$i]-> gal_id ;?>"><?= $lista_galeri[$i]-> gal_name ; ?></option>


                 <? } } ?>
			
						</select>
						
				
					
							
                       
					
					</td>
					
					
              
                </tr>
			
				<tr>
				  <td colspan="2"><button type="submit" class="button submit image-right ic-right-arrow">Stworz</button></td>
				</tr>
				</table>
            
            </tbody>
            </form>
							
							
							<? } ?>
        </div>
    </div>
</div>



<? include "./footer.php" ?>