<? include "./header.php"; 
/*                                                                                                                                                                                                                  
      ___           ___           ___           ___     
     /\  \         /\  \         /\  \         /\__\    
    _\:\  \       /::\  \       |::\  \       /:/ _/_   
   /\ \:\  \     /:/\:\__\      |:|:\  \     /:/ /\  \  
  _\:\ \:\  \   /:/ /:/  /    __|:|\:\  \   /:/ /::\  \ 
 /\ \:\ \:\__\ /:/_/:/__/___ /::::|_\:\__\ /:/_/:/\:\__\
 \:\ \:\/:/  / \:\/:::::/  / \:\~~\  \/__/ \:\/:/ /:/  /
  \:\ \::/  /   \::/~~/~~~~   \:\  \        \::/ /:/  / 
   \:\/:/  /     \:\~~\        \:\  \        \/_/:/  /  
    \::/  /       \:\__\        \:\__\         /:/  /   
     \/__/         \/__/         \/__/         \/__/    
	 
	 # Copyright 2013 by wrcode
     # Feel free to modify the source
     # Don't sell without author permission	 
						
*/
?>

<? $mysql -> admin_check($_SESSION['admin']); ?>

<? $settings = $ustawienia -> settings();  ?>

<div id="header">
    <img src="images/icons/edytor.png" />
    <div class="header-content">
        <span class="header-title">Edytor</span><br />
        <span class="header-tagline">Edytor plików szablonu.</span>
    </div>
</div>
<div id="tabs">

   <a href="index.php?site=main">Strona główna</a>
   <a href="index.php?site=wpisy">Wpisy</a>
   <a href="index.php?site=podstrony">Podstrony</a>
   <a href="index.php?site=galeria">Galeria</a>
   <span>Ustawienia</span>
   <a href="index.php?site=user">Uzytkownicy</a>


   
</div>
<div class="container cf">
    <div class="side-left">
	
	 <div class="side-left-title">Ogolne</div>
        <div class="side-left-content">
            <a href="index.php?site=ustawienia">Ustawienia ogolne</a><br />
            <a href="index.php?site=ustawienia&type=admin">Ustawienia admina</a><br />
			
        </div>
		
		<div class="side-left-title">Wpisy</div>
        <div class="side-left-content">
            <a href="index.php?site=ustawienia&type=wpisy">Ustawienia wpisow</a><br />
        </div>
		
			<div class="side-left-title">Pliki</div>
		
		 <div class="side-left-content">
             <a href="index.php?site=edytor">Podglad plikow</a><br />
        </div>
		
			<div class="side-left-title">Szablon</div>
		
		 <div class="side-left-content">
             <a href="#">Panel boczny</a><br />
			 <a href="#">Informacje</a><br />
        </div>
	
        <div class="side-left-content">
			
				<?

            $list = $edytor->get_file_list($settings['site_theme']);
			 
			for($i=0;$i<count($list);$i++) {
			 
			    echo '<a href="index.php?site=edytor&file='.$list[$i].'">'.$list[$i].'</a><br />';
			 
			}
			 
			?>
			
        </div>
		
			<div class="side-left-title">Menu</div>
        <div class="side-left-content">
            <a href="index.php?site=menu">Menu lista</a><br />
			<a href="index.php?site=menu&action=add">Stworz menu</a><br />
        </div>
	
    </div>
    <div class="side-right">
        <div class="side-right-title">Lista wpisow</div>
        <div class="side-right-content">
		
		<? $settings = $ustawienia -> settings();  ?>
		

		
	<table class="pages-create" width="100%" cellpadding="10px">
      
			
	</table>
	

		
	 
		<table class="pages-create" width="100%" cellpadding="10px">
		
            <form action="index.php?site=ustawienia&type=admin" method="post">
			
			<tr>
			
                <td>
				
				<? if(empty($_GET['file'])) { ?>
				
				<pre class="brush: css;">
				    Witaj  w tym dziale mozesz przegladac pliki szablonu.
					Dzial sluzy wylacznie do podgladu plikow edycja musisz przeprowadzic z poziomu ftp.
					
					Pozdrawiam .wrcode
		        </pre>
				
				
				
				<? } else { ?>
				
				
				
				<pre style="display:none;"class="brush: <?=$edytor -> get_type($_GET['file']); ?>;">
				
				    <? echo $edytor -> get_file($_GET['file'],$settings['site_theme']);?>

		        </pre>
				
				
				<? } ?>
				
			</tr>	
         
			</form>
			
	    </table>
		
	
        </div>
	
								
        </div>
    </div>
</div>



<? include "./footer.php"; ?>