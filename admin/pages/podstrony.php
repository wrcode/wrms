<? include "./header.php";
/*                                                                                                                                                                                                                  
      ___           ___           ___           ___     
     /\  \         /\  \         /\  \         /\__\    
    _\:\  \       /::\  \       |::\  \       /:/ _/_   
   /\ \:\  \     /:/\:\__\      |:|:\  \     /:/ /\  \  
  _\:\ \:\  \   /:/ /:/  /    __|:|\:\  \   /:/ /::\  \ 
 /\ \:\ \:\__\ /:/_/:/__/___ /::::|_\:\__\ /:/_/:/\:\__\
 \:\ \:\/:/  / \:\/:::::/  / \:\~~\  \/__/ \:\/:/ /:/  /
  \:\ \::/  /   \::/~~/~~~~   \:\  \        \::/ /:/  / 
   \:\/:/  /     \:\~~\        \:\  \        \/_/:/  /  
    \::/  /       \:\__\        \:\__\         /:/  /   
     \/__/         \/__/         \/__/         \/__/    
	 
	 # Copyright 2013 by wrcode
     # Feel free to modify the source
     # Don't sell without author permission	 
						
*/
 ?>

<? $mysql -> admin_check($_SESSION['admin']); ?>

<? if(isset($_POST['page_name']) && empty($_GET['edit'])) { $pages -> page_new($_POST['page_name'],$_POST['edytor'],$_POST['page_desc'],$_POST['page_visible'],$_POST['page_type'],$_POST['page_meta_key'],$_POST['page_desc'],$_POST['page_sidebar']);   } ?>

<? if($_GET['delete']) $pages->page_delete($_GET['delete']); ?>

<? if($_GET['edit'] && $_POST['page_name']) { $pages ->page_edit_save($_GET['edit'],$_POST['page_name'],$_POST['edytor'],$_POST['page_desc'],$_POST['post_visible'],$_POST['page_type'],$_POST['page_comments'],$_POST['page_meta_key'],$_POST['page_desc'],$_POST['page_sidebar']); } ?>


<div id="header">
    <img src="images/icons/page.png" />
    <div class="header-content">
        <span class="header-title">Podstrony</span><br />
        <span class="header-tagline">Zarzadzanie podstronami</span>
    </div>
</div>
<div id="tabs">

    <a href="index.php?site=main">Strona główna</a>
    <a href="index.php?site=wpisy">Wpisy</a>
    <span>Podstrony</span>
	<a href="index.php?site=galeria">Galeria</a>
	<a href="index.php?site=ustawienia">Ustawienia</a>
	<a href="index.php?site=user">Uzytkownicy</a>

	
</div>
<div class="container cf">
      <div class="side-left">
	  
	  
        <div class="side-left-title">Podstrony</div>
        <div class="side-left-content">
            <a href="index.php?site=podstrony&action=add">Stworz podstrone</a><br />
            <a href="index.php?site=podstrony">Lista podstron</a><br />
        </div>

		
	
    </div>
    <div class="side-right">
        <div class="side-right-title">Lista podstron</div>
        <div class="side-right-content">
		
	<? if(!isset($_GET['action']) && empty($_GET['edit'])) { ?>	
	
            <table class="pages-list" width="100%" cellpadding="10px">
                <tr class="pages-list-title">
                    <th class="pages-list-id">ID</th>
                    <th class="pages-list-name">Tytul</th>
                    <th class="pages-list-act">Data utworzenia</th>
					<th class="pages-list-act">Data edycji</th>
					<th class="pages-list-act">Dzialania</th>
                </tr>
				
				<? $pages =  $pages -> page(); ?>
				
				<? for($i=0;$i<count($pages);$i++) { ?>
				
                    <tr class="pages-list-item">
	  
                    <td><?=$pages[$i]->page_id; ?></td>
					
                    <td>
                        <a href="#" class="podpowiedz" title="<?=$pages[$i]->page_desc; ?>" target="_blank"><?=$pages[$i]->page_name; ?> </a>
                    </td>
					
					<td style="text-align:center">
					
					<? $date = explode(' ',$pages[$i]->page_date); echo $date[0];  ?>
					
					</td>
					
					<td style="text-align:center">
				
					
					<? $date = explode(' ',$pages[$i]->page_date_edit); echo $date[0];  ?>
					
					</td>
					
                    <td style="text-align:center">
					
                        <a class="podpowiedz" title="Edytuj" href="index.php?site=podstrony&edit=<?=$pages[$i]->page_id; ?>">
						<img src="images/icons/ic_edit.png" />
						</a>
						
						<a class="podpowiedz" title="Usun" href="index.php?site=podstrony&delete=<?=$pages[$i]->page_id; ?>" onclick="if(confirm('Czy napewno chcesz usunac podstrone?')) {} else return false;">
						<img src="images/icons/ic_delete.png" />
						</a>
						
                    </td>
        </tr>
				
				<? } ?>
                            </table>
							
<? } 

	if($_GET['action']=='add' || isset($_GET['edit'] )) 
	{ 
	if(isset($_GET['edit'])) $edit = '&edit='.$_GET['edit'].'';
    $edited = $pages -> page_edit($_GET['edit']); 

							
?> 
							
			<form action="index.php?site=podstrony<?=$edit; ?>" method="post">

            <table class="pages-create" width="100%" cellpadding="10px">
                <input type="hidden" name="type" value="static">
                <tbody>
                <tr>
                    <td>Tytul</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech" value="<?=$edited->page_name; ?>" name="page_name"></td>
					
					<td>Opis</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech" value="<?=$edited->page_desc; ?>" name="page_desc"></td>
					
					 <td>Widocznosc</td>
					 <td>
                        <select name="page_visible">
						<?  
						
						 if($edited -> page_visible == 0) { ?>
						 
						  
						  <option value="0" selected="selected">Tak</option>
					      <option value="1">Nie</option>
						 
						  <? } else { ?>
						  
						  <option value="1" selected="selected">Nie</option>
						  <option value="0">Tak</option>
						  
						  
						  <? } ?>
						
					
							
                        </select>
					</td>	
					
                </tr>
				
				<tr>
				
				      <td>Typ</td>
					 <td>
                        <select name="page_type">
						<?  
						
						 if($edited -> page_type == 0) { ?>
						 
						  <option value="0" selected="selected">Standardowa</option>
						  <option value="1">Szeroka</option>
						 
						  <? } else { ?>
						  
						  <option value="1" selected="selected">Szeroka</option>
					      <option value="0">Standardowa</option>
						  
						  
						  <? } ?>
						
					
							
                        </select>
					</td>
					
					<td>Panel</td>
					 <td>
                        <select name="page_sidebar">
						
							<? $sb = $sidebar ->GetSidebars(); ?>
							<? foreach($sb as $single ) { ?>
								<option value="<?=$single->sidebar_id;?>"><?=$single->sidebar_name;?></option>
							<? } ?>	
						
					
							
                        </select>
					</td>	
				
				</tr>
				
				<tr>
				
				    <td>Meta</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech" value="<?=$edited->page_meta_keys; ?>" name="page_meta_key"></td>
					
					<td>Meta opis</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech" value="<?=$edited->page_meta_desc; ?>" name="page_desc"></td>
				
				</tr>
				
				</table>
				     <table class="pages-create" width="100%" cellpadding="10px">
                <tr>
                    <td>
					
					<textarea  class="ckeditor" id="edytor" name="edytor"><?=$edited->page_content; ?></textarea>
					
					</td>
					
					</tr>
					
					<tr>
					
					 <td colspan="2"><button type="submit" class="button submit image-right ic-right-arrow">Send</button></td>
					
					</tr>
            
            </tbody></table>
            </form>
							
							
							<? } ?>
        </div>
    </div>
</div>



<? include "./footer.php" ?>
