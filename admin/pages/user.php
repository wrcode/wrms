<? include "./header.php";
/*                                                                                                                                                                                                                  
      ___           ___           ___           ___     
     /\  \         /\  \         /\  \         /\__\    
    _\:\  \       /::\  \       |::\  \       /:/ _/_   
   /\ \:\  \     /:/\:\__\      |:|:\  \     /:/ /\  \  
  _\:\ \:\  \   /:/ /:/  /    __|:|\:\  \   /:/ /::\  \ 
 /\ \:\ \:\__\ /:/_/:/__/___ /::::|_\:\__\ /:/_/:/\:\__\
 \:\ \:\/:/  / \:\/:::::/  / \:\~~\  \/__/ \:\/:/ /:/  /
  \:\ \::/  /   \::/~~/~~~~   \:\  \        \::/ /:/  / 
   \:\/:/  /     \:\~~\        \:\  \        \/_/:/  /  
    \::/  /       \:\__\        \:\__\         /:/  /   
     \/__/         \/__/         \/__/         \/__/    
	 
	 # Copyright 2013 by wrcode
     # Feel free to modify the source
     # Don't sell without author permission	 
						
*/
?>

<? $mysql -> admin_check($_SESSION['admin']); ?>

<? if($_POST['user_login']) { $user ->user_edit_save($_GET['edit'],$_POST['user_login'],$_POST['user_email'],$_POST['user_banned'],$_POST['user_active'],$_POST['user_level'],$_POST['user_password']); } ?>

<? if($_GET['site'] == 'user' &&  !empty($_GET['delete'])) { $user -> user_delete($_GET['delete']); } ?>

<div id="header">
    <img src="images/icons/user.png" />
    <div class="header-content">
        <span class="header-title">Uzytkownicy</span><br />
        <span class="header-tagline">Zarzadzanie uzytkownikami</span>
    </div>
</div>
<div id="tabs">

   <a href="index.php?site=main">Strona główna</a>
   <a href="index.php?site=wpisy">Wpisy</a>
   <a href="index.php?site=podstrony">Podstrony</a>
   <a href="index.php?site=galeria">Galeria</a>
   <a href="index.php?site=ustawienia">Ustawienia</a>
   <span>Uzytkownicy</span>


   
</div>
<div class="container cf">
    <div class="side-left">
	
        <div class="side-left-title">Uzytkownicy</div>
        <div class="side-left-content">
		    <a href="index.php?site=user&action=add">Dodaj uzytkownika</a><br />
            <a href="index.php?site=user">Lista uzytkownikow</a><br />
            <a href="index.php?site=user&sort=ban">Lista zbanowanych</a><br />
        </div>
    </div>
    <div class="side-right">
        <div class="side-right-title">Lista wpisow</div>
        <div class="side-right-content">
		
		<? if(!isset($_GET['action']) && empty($_GET['edit'])) { ?>
            <table class="pages-list" width="100%" cellpadding="10px">
                <tr class="pages-list-title">
                    <th class="pages-list-id">ID</th>
                    <th class="pages-list-name">Nazwa </th>
                    <th class="pages-list-pos">Email</th>
					<th class="pages-list-name" style="text-align:center">Data rejestracji</th>
					<th class="pages-list-pos">Aktywny </th>
					<th class="pages-list-pos">Banned</th>
					<th class="pages-list-pos">Level </th>
                    <th class="pages-list-act">Dzialania</th>
                </tr>
				
				
				<?  if(!empty($_GET['sort'])) { $users =  $user -> u('ban'); } else { $users =  $user -> u(''); } ?>
				
				<? for($i=0;$i<count($users);$i++) { ?>
				
                <tr class="pages-list-item">
				
                    <td><?=$users[$i]->user_id; ?></td>
					
                    <td>
                        <a href="#"  target="_blank"><?=$users[$i]->user_login; ?></a>
                    </td>
					
					<td style="text-align:center">
					<?=$users[$i]->user_email; ?>
					</td>
					
					<td style="text-align:center">
					<?=$users[$i]->user_register_date; ?>
					</td>
					
					<td style="text-align:center">
					<? if($users[$i]->user_active == 1) { echo 'Tak'; } else  { echo 'Nie'; } ?>
					</td>
					
					<td style="text-align:center">
					<? if($users[$i]->user_banned == 1) { echo 'Tak'; } else  { echo 'Nie'; } ?>
					</td>
					
						<td style="text-align:center">
					<?=$user->get_user_level($users[$i]->user_level); ?>
					</td>
					
					
					
                <td style="text-align:center">
					
					    <a href="index.php?site=user&delete=<?=$users[$i]->user_id; ?>" onclick="if(confirm('Czy napewno chcesz usunac uzytkownika?')) {} else return false;">
						<img src="images/icons/ic_delete.png" />
						</a>
					
                        <a href="index.php?site=user&edit=<?=$users[$i]->user_id; ?>" >
						<img src="images/icons/ic_edit.png" />
						</a>
						
						
						
                </td>
                </tr>
				
				<? } ?>
                            </table>
							
<? } 

	if($_GET['action']=='add' || isset($_GET['edit'] )) 
	{ 
	if(isset($_GET['edit'])) $edit = '&edit='.$_GET['edit'].'';
    $edited = $user -> user_edit($_GET['edit']); 
							
?> 
							
		<form action="index.php?site=user<?=$edit; ?>" method="post">

            <table class="pages-create" width="100%" cellpadding="10px">
                <input type="hidden" name="type" value="static">
                <tbody>
                <tr>
                    <td>Login</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech" value="<?=$edited->user_login; ?>" name="user_login"></td>
					
				
					
					<td>Email</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech" value="<?=$edited->user_email; ?>" name="user_email"></td>
					
					
				</tr>	
				<tr>
					<td>Ban</td>
                    <td>
                        <select name="user_banned">
						<?  
						
						 if($edited -> user_banned == 0) { ?>
						 
						   <option value="1" selected="selected">Tak</option>
						  <option value="0" selected="">Nie</option>
						 
						  <? } else { ?>
						  
						   <option value="0" selected="selected">Nie</option>
					      <option value="1" selected="">Tak</option>
						  
						  
						  <? } ?>
						
					
							
                        </select>
						
                    </td>
					
				
				 <td>Aktywny</td>
                    <td>
                        <select name="user_active">
						<?  
						
						 if($edited -> user_active == 0) { ?>
						 
						  <option value="1" selected="selected">Tak</option>
						  <option value="0" selected="">Nie</option>
						 
						  <? } else { ?>
						  
						  <option value="0" selected="selected">Nie</option>
					      <option value="1" selected="">Tak</option>
						  
						  
						  <? } ?>
						
					
							
                        </select>
						
                    </td>
					
				</tr>
				
				<tr>
				
				 <td>Level</td>
				 
                    <td>
					
                        <select name="user_level">
				
					
					        <option <?  if($edited -> user_level == 0) { echo 'selected = "selected"'; } ?> value="0">User</option>	
							<option <?  if($edited -> user_level == 1) { echo 'selected = "selected"'; } ?> value="1">Super user</option>
                            <option <?  if($edited -> user_level == 2) { echo 'selected = "selected"'; } ?> value="2">Moderator</option>
                            <option <?  if($edited -> user_level == 3) { echo 'selected = "selected"'; } ?> value="3">Administrator</option>							
					
							
                        </select>
						
						
                    </td>
					
				
				        <td>Haslo</td>
						<td><input type="text" value="" placeholder="***********************" class="podpowiedz" title="Pozostaw puste jesli chcesz by nie zostalo zmienione" name="user_password"></td>
				
				</tr>
				
                   
   
                 <tr>
                    <td colspan="2"><button type="submit" name="admin_submit"  class="button submit image-right ic-right-arrow">Aktualizuj</button></td>
                </tr>
				
				</table>
				
            
            </tbody>
            </form>
							
							
							<? } ?>
        </div>
    </div>
</div>



<? include "./footer.php" ?>