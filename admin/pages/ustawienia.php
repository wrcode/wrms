<? include "./header.php";
/*                                                                                                                                                                                                                  
      ___           ___           ___           ___     
     /\  \         /\  \         /\  \         /\__\    
    _\:\  \       /::\  \       |::\  \       /:/ _/_   
   /\ \:\  \     /:/\:\__\      |:|:\  \     /:/ /\  \  
  _\:\ \:\  \   /:/ /:/  /    __|:|\:\  \   /:/ /::\  \ 
 /\ \:\ \:\__\ /:/_/:/__/___ /::::|_\:\__\ /:/_/:/\:\__\
 \:\ \:\/:/  / \:\/:::::/  / \:\~~\  \/__/ \:\/:/ /:/  /
  \:\ \::/  /   \::/~~/~~~~   \:\  \        \::/ /:/  / 
   \:\/:/  /     \:\~~\        \:\  \        \/_/:/  /  
    \::/  /       \:\__\        \:\__\         /:/  /   
     \/__/         \/__/         \/__/         \/__/    
	 
	 # Copyright 2013 by wrcode
     # Feel free to modify the source
     # Don't sell without author permission	 
						
*/
 ?>

<? $mysql -> admin_check($_SESSION['admin']); ?>

<? if($_POST['admin_login'])  $ustawienia -> admin_set($_POST['admin_login'],$_POST['admin_pass'],$_POST['admin_pass_re'],$_POST['admin_pass_new'],$_POST['admin_email']); ?>

<? if($_POST['site_title'])  $ustawienia ->admin_ogolne($_POST['site_title'],$_POST['site_meta_keywords'],$_POST['site_meta_description'],$_POST['site_meta_author'],$_POST['site_meta_robots'],$_POST['site_online']); ?>

<div id="header">
    <img src="images/icons/settings.png" />
    <div class="header-content">
        <span class="header-title">Ustawienia</span><br />
        <span class="header-tagline">Zarzadzanie strona</span>
    </div>
</div>
<div id="tabs">

   <a href="index.php?site=main">Strona główna</a>
   <a href="index.php?site=wpisy">Wpisy</a>
   <a href="index.php?site=podstrony">Podstrony</a>
   <a href="index.php?site=galeria">Galeria</a>
   <span>Ustawienia</span>
   <a href="index.php?site=user">Uzytkownicy</a>


   
</div>
<div class="container cf">
    <div class="side-left">
	
        <div class="side-left-title">Ogolne</div>
        <div class="side-left-content">
            <a href="index.php?site=ustawienia">Ustawienia ogolne</a><br />
            <a href="index.php?site=ustawienia&type=admin">Ustawienia admina</a><br />
			
        </div>
		
		<div class="side-left-title">Wpisy</div>
        <div class="side-left-content">
            <a href="index.php?site=ustawienia&type=wpisy">Ustawienia wpisow</a><br />
        </div>
		
		
		<div class="side-left-title">Szablon</div>
		
		 <div class="side-left-content">
             <a href="index.php?site=szablon">Panel boczny</a><br />
			 <a href="#">Informacje</a><br />
        </div>
		
		<div class="side-left-title">Menu</div>
        <div class="side-left-content">
            <a href="index.php?site=menu">Menu lista</a><br />
			<a href="index.php?site=menu&action=add">Stworz menu</a><br />
        </div>
		
		

		
    </div>
    <div class="side-right">
        <div class="side-right-title">Lista wpisow</div>
        <div class="side-right-content">
		
		<? $settings = $ustawienia -> settings();  ?>
		
		<? if($_GET['site'] == 'ustawienia' && empty($_GET['type'])) { ?>
		
	<table class="pages-create" width="100%" cellpadding="10px">
            <form action="index.php?site=ustawienia" method="post">
                <tbody>
				<tr>
                    <td>Nazwa strony</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech" name="site_title" value="<?=$settings['site_title'];?>" class="podpowiedz" title="Tytul strony np : Nazwa twojej firmy" ></td>
                </tr>
				
				 <tr>
			          <td>Autor</td>
				      <td><input type="text"  x-webkit-speech="x-webkit-speech"  x-webkit-speech="x-webkit-speech" name="site_meta_author" value="<?=$settings['site_meta_author'];?>" class="podpowiedz" title="Nazwa autora dla wyszukiwarek." ></td>
			   </tr>
			   
			    <tr>
			          <td>Robots</td>
				      <td><input type="text"  x-webkit-speech="x-webkit-speech"  x-webkit-speech="x-webkit-speech" name="site_meta_robots" value="<?=$settings['site_meta_robots'];?>" class="podpowiedz" title="Ustawienia dla robotow co do indexowania strony." ></td>
			   </tr>
			   
			   	<tr>
                    <td>Slowa kluczowe</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech"  x-webkit-speech="x-webkit-speech" name="site_meta_keywords" value="<?=$settings['site_meta_keywords'];?>" style="width:300px;" class="podpowiedz" title="Tagi musza byc odzielone przecinkami. Max 5,6 tagow."></td>
                </tr>
					<tr>
				<td>Wylaczanie</td>
				<td>
				    <select class="podpowiedz" title="Wylaczanie i wlaczanie strony dla odwiedzajacych" name="site_online">
						<?  
						
						 if($settings['site_online'] == 0) { ?>
						 
						  
						  <option value="0" selected="selected">Wylaczona</option>
					      <option value="1">Wlaczona</option>
						 
						  <? } else { ?>
						  
						  <option value="1" selected="selected">Wlaczona</option>
						  <option value="0">Wylaczona</option>
						  
						  
						  <? } ?>
						
					
							
                        </select>
				</td>		
                </tr>
				<tr>
                    <td style="vertical-align:text-top;">Meta opis</td>
					<td><textarea   name="site_meta_description" class="podpowiedz" title="Opis strony powinien nie przekraczac 255 znakow. Jest on wazy dla wyszukiwarek ala google"><?=$settings['site_meta_description'];?></textarea></td>
                </tr>
				
			
       

			  <tr>
                    <td colspan="2"><button type="submit" name="admin_ogolne" class="button submit image-right ic-right-arrow">Aktualizuj</button></td>
                </tr>
            
            </tbody>
			
			</form>
			
	</table>
	
	<? } ?>

	
		<?  if($_GET['type'] == 'wpisy') { ?>
	
	
		<table class="pages-create" width="100%" cellpadding="10px">
            <form action="index.php?site=ustawienia&type=admin" method="post">
                <tbody>
				<tr>
                    <td>Dlugosc tytulu</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech" name="admin_login" value="<?=$settings["site_title_lenght"]?>" ></td>
                </tr>
				
				<tr>
                    <td>Dlugosc tresci</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech" name="site_post_lenght" value="<?=$settings["site_post_lenght"]?>" ></td>
                </tr>
				
				<tr>
				
				    <td>Sidebar na postronie wpisow</td>
					
					<td>
					
                    <select name="site_post_sidebar">
					
					
						<?  
						
						 if($settings["site_post_sidebar"] == 0) { ?>
						 
						  
						  <option value="0" selected="selected">Nie</option>
					      <option value="1">Tak</option>
						 
						  <? } else { ?>
						  
						  <option value="1" selected="selected">Tak</option>
						  <option value="0">Nie</option>
						  
						  
						  <? } ?>
						
					
							
                        </select>
						</td>
                </tr>
				
				<tr>
				
				    <td>Sidebar na liscie wpisow</td>
					
					<td>
					
                    <select name="site_posts_sidebar">
					
					
						<?  
						
						 if($settings["site_posts_sidebar"] == 0) { ?>
						 
						  
						  <option value="0" selected="selected">Nie</option>
					      <option value="1">Tak</option>
						 
						  <? } else { ?>
						  
						  <option value="1" selected="selected">Tak</option>
						  <option value="0">Nie</option>
						  
						  
						  <? } ?>
						
					
							
                        </select>
						</td>
                </tr>
				
                <tr>
                    <td colspan="2"><button type="submit" name="admin_submit"  class="button submit image-right ic-right-arrow">Aktualizuj</button></td>
                </tr>
				
            </tbody>
			
			</form>
			
	</table>
	
	<? } ?>
							
					
        </div>
    </div>
</div>



<? include "./footer.php" ?>