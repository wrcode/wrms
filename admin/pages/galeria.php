<? include "./header.php";
/*                                                                                                                                                                                                                  
      ___           ___           ___           ___     
     /\  \         /\  \         /\  \         /\__\    
    _\:\  \       /::\  \       |::\  \       /:/ _/_   
   /\ \:\  \     /:/\:\__\      |:|:\  \     /:/ /\  \  
  _\:\ \:\  \   /:/ /:/  /    __|:|\:\  \   /:/ /::\  \ 
 /\ \:\ \:\__\ /:/_/:/__/___ /::::|_\:\__\ /:/_/:/\:\__\
 \:\ \:\/:/  / \:\/:::::/  / \:\~~\  \/__/ \:\/:/ /:/  /
  \:\ \::/  /   \::/~~/~~~~   \:\  \        \::/ /:/  / 
   \:\/:/  /     \:\~~\        \:\  \        \/_/:/  /  
    \::/  /       \:\__\        \:\__\         /:/  /   
     \/__/         \/__/         \/__/         \/__/    
	 
	 # Copyright 2013 by wrcode
     # Feel free to modify the source
     # Don't sell without author permission	 
						
*/
?>

<? $mysql -> admin_check($_SESSION['admin']); ?>

<? if(isset($_POST['gal_name']) && empty($_GET['edit'])) $gallery ->gallery_new($_POST['gal_name'],$_POST['gal_desc']); ?>

<? if($_GET['delete']) $gallery -> gallery_delete($_GET['delete']); ?>

<? if($_GET['edit'] && $_POST['gal_name']) $gallery ->gallery_edit_save($_GET['edit'],$_POST['gal_name'],$_POST['gal_desc']);  ?>
<div id="header">
    <img src="images/icons/gallery.png" />
    <div class="header-content">
        <span class="header-title">Galeria</span><br />
        <span class="header-tagline">Zarzadzanie galeria</span>
    </div>
</div>
<div id="tabs">

   <a href="index.php?site=main">Strona główna</a>
   <a href="index.php?site=wpisy">Wpisy</a>
   <a href="index.php?site=podstrony">Podstrony</a>
   <span>Galeria</span>
   <a href="index.php?site=ustawienia">Ustawienia</a>
   <a href="index.php?site=user">Uzytkownicy</a>


   
</div>
<div class="container cf">
    <div class="side-left">
	
        <div class="side-left-title">Galeria</div>
        <div class="side-left-content">
            <a href="index.php?site=galeria&action=add">Stworz galerie</a><br />
            <a href="index.php?site=galeria">Lista galeri</a><br />
        </div>
		
		<div class="side-left-title">Obrazy</div>
        <div class="side-left-content">
            <a href="index.php?site=obraz&action=add">Dodaj obraz</a><br />
			<a href="index.php?site=obraz">Lista obrazow</a><br />
        </div>
	
		
    </div>
    <div class="side-right">
        <div class="side-right-title">Lista galeri</div>
        <div class="side-right-content">
		
		<? if(!isset($_GET['action']) && empty($_GET['edit'])) { ?>
        
		<ul class="gallery">
		
			
		<? $gallerys =  $gallery -> gallerys(); ?>
				
		<? for($i=0;$i<count($gallerys);$i++) { ?>
		
		
		<li>
				<span><?=$gallerys[$i] -> gal_name;  ?></span>
		<div class="img">
		<a href="index.php?site=obraz&gallery_id=<?=$gallerys[$i] -> gal_id;  ?>" class="podpowiedz" title="<?=$gallerys[$i] -> gal_desc;  ?>"><img src="<?=$gallerys[$i] -> gal_thumbnail;  ?>" width="100" height="100" /></a>
		</div>
        <a class="podpowiedz" title="Usun galerie" href="index.php?site=galeria&delete=<?=$gallerys[$i] -> gal_id;  ?>" onclick="if(confirm('Czy napewno chcesz usunac galerie?')) {} else return false;"><img src="images/icons/ic_delete.png"></a>
		<a class="podpowiedz" title="Edytuj galerie" href="index.php?site=galeria&edit=<?=$gallerys[$i] -> gal_id;  ?>"><img src="images/icons/ic_edit.png"></a>
        </li>
		
	    <? } ?>
		
		
		</ul>
		
				
		<? } ?>
                           
							
<? 

	if($_GET['action']=='add' || isset($_GET['edit'] )) 
	{ 
	if(isset($_GET['edit'])) $edit = '&edit='.$_GET['edit'].'';
    $edited = $gallery -> gallery_edit($_GET['edit']); 
							
?> 
							
		<form action="index.php?site=galeria<?=$edit; ?>" method="post">

            <table class="pages-create" width="100%" cellpadding="10px">
                <input type="hidden" name="type" value="static">
                <tbody>
                <tr>
				
                    <td>Tytul</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech" value="<?=$edited->gal_name; ?>" name="gal_name"></td>
					
					<td>Opis</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech" value="<?=$edited->gal_desc; ?>" name="gal_desc"></td>
					
					
                <td colspan="2"><button type="submit" class="button submit image-right ic-right-arrow">Stworz</button></td>
                </tr>
			
				
				</table>
            
            </tbody>
            </form>
							
							
							<? } ?>
        </div>
    </div>
</div>



<? include "./footer.php" ?>