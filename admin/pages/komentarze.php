<? include "./header.php";
/*                                                                                                                                                                                                                  
      ___           ___           ___           ___     
     /\  \         /\  \         /\  \         /\__\    
    _\:\  \       /::\  \       |::\  \       /:/ _/_   
   /\ \:\  \     /:/\:\__\      |:|:\  \     /:/ /\  \  
  _\:\ \:\  \   /:/ /:/  /    __|:|\:\  \   /:/ /::\  \ 
 /\ \:\ \:\__\ /:/_/:/__/___ /::::|_\:\__\ /:/_/:/\:\__\
 \:\ \:\/:/  / \:\/:::::/  / \:\~~\  \/__/ \:\/:/ /:/  /
  \:\ \::/  /   \::/~~/~~~~   \:\  \        \::/ /:/  / 
   \:\/:/  /     \:\~~\        \:\  \        \/_/:/  /  
    \::/  /       \:\__\        \:\__\         /:/  /   
     \/__/         \/__/         \/__/         \/__/    
	 
	 # Copyright 2013 by wrcode
     # Feel free to modify the source
     # Don't sell without author permission	 
						
*/
 ?>

<? $mysql -> admin_check($_SESSION['admin']); ?>

<? if($_GET['delete']) $post->comment_delete($_GET['delete']); ?>

<? if($_GET['edit'] && $_POST['cm_author'] ) $post ->comment_edit_save($_GET['edit'],$_POST['cm_author'],$_POST['edytor'],$_POST['cm_author_email'],$_POST['banned']);  ?>

<div id="header">
    <img src="images/icons/content.png" />
    <div class="header-content">
        <span class="header-title">Wpisy</span><br />
        <span class="header-tagline">Zarzadzanie wpisami</span>
    </div>
</div>
<div id="tabs">

   <a href="index.php?site=main">Strona główna</a>
   <span>Wpisy</span>
   <a href="index.php?site=podstrony">Podstrony</a>
   <a href="index.php?site=galeria">Galeria</a>
   <a href="index.php?site=ustawienia">Ustawienia</a>
   <a href="index.php?site=user">Uzytkownicy</a>


   
</div>
<div class="container cf">
    <div class="side-left">
	
        <div class="side-left-title">Wpisy</div>
        <div class="side-left-content">
            <a href="index.php?site=wpisy&action=add">Stworz wpis</a><br />
            <a href="index.php?site=wpisy">Lista wpisow</a><br />
        </div>
		
		<div class="side-left-title">Kategorie</div>
        <div class="side-left-content">
            			<a href="index.php?site=kategorie">Lista kategori</a><br />
			<a href="index.php?site=kategorie&action=add">Stworz kategorie</a><br />
        </div>
		
		<div class="side-left-title">Komentarze</div>
        <div class="side-left-content">
            			<a href="index.php?site=komentarze">Lista komentarzy</a><br />
        </div>
		
    </div>
    <div class="side-right">
        <div class="side-right-title">Lista komentarzy</div>
        <div class="side-right-content">
		
		<? if(!isset($_GET['action']) && empty($_GET['edit'])) { ?>
            <table class="pages-list" width="100%" cellpadding="10px">
                <tr class="pages-list-title">
                    <th class="pages-list-id">ID</th>
                    <th class="pages-list-name">Tytul</th>
                    <th class="pages-list-pos">Banned</th>
					<th class="pages-list-pos">Data utworzenia </th>
					<th class="pages-list-pos">Data edycji </th>
                    <th class="pages-list-act">Dzialania</th>
                </tr>
				
				
				<? $comments  =  $post -> comments(); ?>
				
				<? for($i=0;$i<count($comments);$i++) { ?>
				
                <tr class="pages-list-item">
				
                    <td><?=$comments[$i]->cm_id; ?></td>
					
                    <td>
                        <a href="#" class="podpowiedz" title="<?=$posts[$i]->post_description;   ?>" target="_blank"><?=$comments[$i]->cm_author; ?></a>
                    </td>
					
					<td style="text-align:center">
					<?=$post -> post_visible($posts[$i]->post_visible); ?>
					</td>
					
					<td style="text-align:center">
					<?=$comments[$i]->cm_date; ?>
					</td>
					
						<td style="text-align:center">
					<?=$comments[$i]->cm_date_edit; ?>
					</td>
					
					
					
                <td style="text-align:center">
					
					    <a href="index.php?site=komentarze&delete=<?=$comments[$i]->cm_id; ?>" onclick="if(confirm('Czy napewno chcesz usunac komentarz?')) {} else return false;">
						<img src="images/icons/ic_delete.png" />
						</a>
					
                        <a href="index.php?site=komentarze&edit=<?=$comments[$i]->cm_id; ?>" >
						<img src="images/icons/ic_edit.png" />
						</a>
						
						
						
                </td>
                </tr>
				
				<? } ?>
                            </table>
							
<? } 

	if($_GET['action']=='add' || isset($_GET['edit'] )) 
	{ 
	if(isset($_GET['edit'])) $edit = '&edit='.$_GET['edit'].'';
    $edited = $post -> comment_edit($_GET['edit']); 
							
?> 
							
		<form action="index.php?site=komentarze<?=$edit; ?>" method="post">

            <table class="pages-create" width="100%" cellpadding="10px">
                <input type="hidden" name="type" value="static">
                <tbody>
                <tr>
                    <td>Autor</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech" value="<?=$edited->cm_author; ?>" name="cm_author"></td>
					
					<td>Email</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech" value="<?=$edited->cm_author_email; ?>" name="cm_author_email"></td>
					
				
						   <td>Banned</td>
                    <td>
                        <select name="banned">
						<?  
						
						 if($edited -> cm_banned == 0) { ?>
						 
						  <option value="1" selected="selected">Tak</option>
						  <option value="0" selected="">Nie</option>
						 
						  <? } else { ?>
						  
						  <option value="0" selected="selected">Nie</option>
					      <option value="1" selected="">Tak</option>
						  
						  
						  <? } ?>
						
					
							
                        </select>
						
                    </td>
					
                   
   
                </tr>
				
				</table>
				     <table class="pages-create" width="100%" cellpadding="10px">
                <tr>
                    <td>
					
					<textarea  class="ckeditor" id="edytor" name="edytor"><?=$edited->cm_content; ?></textarea>
					
					</td>
					
					</tr>
					
					<tr>
					
					 <td colspan="2"><button type="submit" name="edytuj" class="button submit image-right ic-right-arrow">Send</button></td>
					
					</tr>
            
            </tbody></table>
            </form>
							
							
							<? } ?>
        </div>
    </div>
</div>



<? include "./footer.php" ?>