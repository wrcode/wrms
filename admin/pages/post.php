<? include "./header.php"; 
/*                                                                                                                                                                                                                  
      ___           ___           ___           ___     
     /\  \         /\  \         /\  \         /\__\    
    _\:\  \       /::\  \       |::\  \       /:/ _/_   
   /\ \:\  \     /:/\:\__\      |:|:\  \     /:/ /\  \  
  _\:\ \:\  \   /:/ /:/  /    __|:|\:\  \   /:/ /::\  \ 
 /\ \:\ \:\__\ /:/_/:/__/___ /::::|_\:\__\ /:/_/:/\:\__\
 \:\ \:\/:/  / \:\/:::::/  / \:\~~\  \/__/ \:\/:/ /:/  /
  \:\ \::/  /   \::/~~/~~~~   \:\  \        \::/ /:/  / 
   \:\/:/  /     \:\~~\        \:\  \        \/_/:/  /  
    \::/  /       \:\__\        \:\__\         /:/  /   
     \/__/         \/__/         \/__/         \/__/    
	 
	 # Copyright 2013 by wrcode
     # Feel free to modify the source
     # Don't sell without author permission	 
						
*/
?>

<? $mysql -> admin_check($_SESSION['admin']); ?>

<? if(isset($_POST['title']) && empty($_GET['edit'])) $post ->post_new($_POST['title'],$_POST['edytor'],$_POST['description'],$_POST['visible'],$_POST['kategoria']); ?>

<? if($_GET['delete']) $post->post_delete($_GET['delete']); ?>

<? if($_GET['edit'] && $_POST['title']) $post ->post_edit_save($_GET['edit'],$_POST['title'],$_POST['edytor'],$_POST['description'],$_POST['visible'],$_POST['kategoria']);  ?>

<div id="header">
    <img src="images/icons/content.png" />
    <div class="header-content">
        <span class="header-title">Wpisy</span><br />
        <span class="header-tagline">Zarzadzanie wpisami</span>
    </div>
</div>
<div id="tabs">

   <a href="index.php?site=main">Strona główna</a>
   <span>Wpisy</span>
   <a href="index.php?site=podstrony">Podstrony</a>
   <a href="index.php?site=galeria">Galeria</a>
   <a href="index.php?site=ustawienia">Ustawienia</a>
   <a href="index.php?site=user">Uzytkownicy</a>


   
</div>
<div class="container cf">
    <div class="side-left">
	
        <div class="side-left-title">Wpisy</div>
        <div class="side-left-content">
            <a href="index.php?site=wpisy&action=add">Stworz wpis</a><br />
            <a href="index.php?site=wpisy">Lista wpisow</a><br />
        </div>
		
		<div class="side-left-title">Kategorie</div>
        <div class="side-left-content">
            			<a href="index.php?site=kategorie">Lista kategori</a><br />
			<a href="index.php?site=kategorie&action=add">Stworz kategorie</a><br />
        </div>
		
		
    </div>
    <div class="side-right">
        <div class="side-right-title">Lista wpisow</div>
        <div class="side-right-content">
		
		<? if(!isset($_GET['action']) && empty($_GET['edit'])) { ?>
            <table class="pages-list" width="100%" cellpadding="10px">
                <tr class="pages-list-title">
                    <th class="pages-list-id">ID</th>
                    <th class="pages-list-name">Tytul</th>
                    <th class="pages-list-pos">Widocznosc</th>
					<th class="pages-list-pos">Data utworzenia </th>
					<th class="pages-list-pos">Data edycji </th>
					<th class="pages-list-name" >Kategoria</th>
                    <th class="pages-list-act">Dzialania</th>
                </tr>
				
				
				<? $posts =  $post -> posts(); ?>
				
				<? for($i=0;$i<count($posts);$i++) { ?>
				
                <tr class="pages-list-item">
				
                    <td><?=$posts[$i]->post_id; ?></td>
					
                    <td>
                        <a href="#" class="podpowiedz" title="<?=$posts[$i]->post_description;   ?>" target="_blank"><?=$posts[$i]->post_name; ?></a>
                    </td>
					
					<td style="text-align:center">
					<?=$post -> post_visible($posts[$i]->post_visible); ?>
					</td>
					
					<td style="text-align:center">
					<?=$posts[$i]->post_date; ?>
					</td>
					
						<td style="text-align:center">
					<?=$posts[$i]->post_last_edit; ?>
					</td>
					
					
                    <td style="text-align:center">
					<?=$post->post_category($posts[$i]->post_category); ?>
                    </td>
					
                <td style="text-align:center">
					
					    <a href="index.php?site=wpisy&delete=<?=$posts[$i]->post_id; ?>" onclick="if(confirm('Czy napewno chcesz usunac wpis?')) {} else return false;">
						<img src="images/icons/ic_delete.png" />
						</a>
					
                        <a href="index.php?site=wpisy&edit=<?=$posts[$i]->post_id; ?>" >
						<img src="images/icons/ic_edit.png" />
						</a>
						
						
						
                </td>
                </tr>
				
				<? } ?>
                            </table>
							
<? } 

	if($_GET['action']=='add' || isset($_GET['edit'] )) 
	{ 
	if(isset($_GET['edit'])) $edit = '&edit='.$_GET['edit'].'';
    $edited = $post -> post_edit($_GET['edit']); 
							
?> 
							
		<form action="index.php?site=wpisy<?=$edit; ?>" method="post">

            <table class="pages-create" width="100%" cellpadding="10px">
                <input type="hidden" name="type" value="static">
                <tbody>
                <tr>
                    <td>Tytul</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech"  x-webkit-speech="x-webkit-speech" value="<?=$edited->post_name; ?>" name="title"></td>
					
					<td>Opis</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech" value="<?=$edited->post_description; ?>" name="description"></td>
					
				
						   <td>Widocznosc</td>
                    <td>
                        <select name="visible">
						<?  
						
						 if($edited -> post_visible == 0) { ?>
						 
						  <option value="1" selected="selected">Tak</option>
						  <option value="0" selected="">Nie</option>
						 
						  <? } else { ?>
						  
						  <option value="0" selected="selected">Nie</option>
					      <option value="1" selected="">Tak</option>
						  
						  
						  <? } ?>
						
					
							
                        </select>
						
                    </td>
					
                   
   
                </tr>
				<tr>
				
				
					
					 <td>Kategoria</td>
					   <td>
					   <select name="kategoria">
					   
						
						    <? $kategorie =  $category -> categorys(); ?>
							
							<? for($i=0;$i<count($kategorie);$i++) { ?>
							
							<? if($edited->post_category ==  $kategorie[$i]->cat_id) { ?>
							
							<option selected="selected" value="<?php echo $kategorie[$i]->cat_id; ?>"><?php echo $kategorie[$i]->cat_name; ?> ( Obecna )</option>
							
							
							<? } else { ?>
							
							<option value="<?php echo $kategorie[$i]->cat_id; ?>"><?php echo $kategorie[$i]->cat_name; ?></option>
							
							
							
							<? } } ?>
							
                        </select>
				  </td>
				</tr>
				
				</table>
				     <table class="pages-create" width="100%" cellpadding="10px">
                <tr>
                    <td>
					
					<textarea class="ckeditor"  id="edytor" name="data[post][edytor]"><?=$edited->post_content; ?></textarea>
					
					</td>
					
					</tr>
					
					<tr>
					
					 <td colspan="2"><button type="submit" class="button submit image-right ic-right-arrow">Send</button></td>
					
					</tr>
            
            </tbody>
			</table>

            </form>
							
							
							<? } ?>
        </div>
    </div>
</div>



<? include "./footer.php" ?>