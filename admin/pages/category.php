<? include "./header.php";
/*                                                                                                                                                                                                                  
      ___           ___           ___           ___     
     /\  \         /\  \         /\  \         /\__\    
    _\:\  \       /::\  \       |::\  \       /:/ _/_   
   /\ \:\  \     /:/\:\__\      |:|:\  \     /:/ /\  \  
  _\:\ \:\  \   /:/ /:/  /    __|:|\:\  \   /:/ /::\  \ 
 /\ \:\ \:\__\ /:/_/:/__/___ /::::|_\:\__\ /:/_/:/\:\__\
 \:\ \:\/:/  / \:\/:::::/  / \:\~~\  \/__/ \:\/:/ /:/  /
  \:\ \::/  /   \::/~~/~~~~   \:\  \        \::/ /:/  / 
   \:\/:/  /     \:\~~\        \:\  \        \/_/:/  /  
    \::/  /       \:\__\        \:\__\         /:/  /   
     \/__/         \/__/         \/__/         \/__/    
	 
	 # Copyright 2013 by wrcode
     # Feel free to modify the source
     # Don't sell without author permission	 
						
*/
?>

<? $mysql -> admin_check($_SESSION['admin']); ?>

<? if(isset($_POST['cat_name']) && empty($_GET['edit'])) $category ->cat_new($_POST['cat_name'],$_POST['cat_desc']); ?>

<? if($_GET['edit'] && $_POST['cat_name']) $category ->cat_edit_save($_GET['edit'],$_POST['cat_name'],$_POST['cat_desc']); ?>

<? if($_GET['delete']) $category -> category_delete($_GET['delete']); ?>

<div id="header">
     <img src="images/icons/content.png" />
    <div class="header-content">
        <span class="header-title">Wpisy</span><br />
        <span class="header-tagline">Zarzadzanie wpisam</span>
    </div>
</div>
<div id="tabs">
   <a href="index.php?site=main">Strona główna</a>
   <span>Wpisy</span>
   <a href="index.php?site=podstrony">Podstrony</a>
   <a href="index.php?site=galeria">Galeria</a>
   <a href="index.php?site=ustawienia">Ustawienia</a>
   <a href="index.php?site=user">Uzytkownicy</a>

	
</div>
<div class="container cf">
      <div class="side-left">
	  
        <div class="side-left-title">Wpisy</div>
        <div class="side-left-content">
            <a href="index.php?site=wpisy&action=add">Stworz wpis</a><br />
            <a href="index.php?site=wpisy">Lista wpisow</a><br />
        </div>

		
		<div class="side-left-title">Kategorie</div>
        <div class="side-left-content">
            <a href="index.php?site=kategorie">Lista kategori</a><br />
			<a href="index.php?site=kategorie&action=add">Stworz kategorie</a><br />
        </div>
		
		
	
    </div>
    <div class="side-right">
        <div class="side-right-title">Lista kategori</div>
        <div class="side-right-content">
		
	<? if(!isset($_GET['action']) && empty($_GET['edit'])) { ?>	
	
            <table class="pages-list" width="100%" cellpadding="10px">
                <tr class="pages-list-title">
                    <th class="pages-list-id">ID</th>
                    <th class="pages-list-name">Tytul</th>
                    <th class="pages-list-act">Data utworzenia</th>
					<th class="pages-list-act">Data edycji</th>
					<th class="pages-list-act">Dzialania</th>
                </tr>
				
				<? $categorys =  $category -> categorys(); ?>
				
				<? for($i=0;$i<count($categorys);$i++) { ?>
				
        <tr class="pages-list-item">
	  
                    <td><?=$categorys[$i]->cat_id; ?></td>
					
                    <td>
                        <a href="#" class="podpowiedz" title="<?=$categorys[$i]->cat_desc; ?>" target="_blank"><?=$categorys[$i]->cat_name; ?></a>
                    </td>
					
					<td style="text-align:center">
					
					<?=$categorys[$i]->cat_date; ?>
					
					</td>
					
					<td style="text-align:center">
					
					<?=$categorys[$i]->cat_date_edit; ?>
					
					</td>
					
                    <td style="text-align:center">
					
                        <a href="index.php?site=kategorie&edit=<?=$categorys[$i]->cat_id; ?>">
						<img src="images/icons/ic_edit.png" />
						</a>
						
						 <a href="index.php?site=kategorie&delete=<?=$categorys[$i]->cat_id; ?>" onclick="if(confirm('Czy napewno chcesz usunac kategorie?')) {} else return false;">
						<img src="images/icons/ic_delete.png" />
						</a>
						
                    </td>
        </tr>
				
				<? } ?>
                            </table>
							
<? } 

	if($_GET['action']=='add' || isset($_GET['edit'] )) 
	{ 
	if(isset($_GET['edit'])) $edit = '&edit='.$_GET['edit'].'';
    $edited = $category -> category_edit($_GET['edit']); 
							
?> 
							
		<form action="index.php?site=kategorie<?=$edit; ?>" method="post">

            <table class="pages-create" width="100%" cellpadding="10px">
                <input type="hidden" name="type" value="static">
                <tbody>
                <tr>
                    <td>Nazwa</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech" value="<?=$edited->cat_name; ?>" name="cat_name"></td>
					
					<td>Opis</td>
                    <td><input type="text"  x-webkit-speech="x-webkit-speech" value="<?=$edited->cat_desc; ?>" name="cat_desc"></td>
					
                   <td colspan="2"><button type="submit" class="button submit image-right ic-right-arrow">Stworz</button></td>
   
                </tr>
				
				</table>
				    
        
            
            </tbody>
            </form>
							
							
							<? } ?>
        </div>
    </div>
</div>



<? include "./footer.php" ?>
