<? include "./header.php"; 
error_reporting(1);
/*                                                                                                                                                                                                                  
      ___           ___           ___           ___     
     /\  \         /\  \         /\  \         /\__\    
    _\:\  \       /::\  \       |::\  \       /:/ _/_   
   /\ \:\  \     /:/\:\__\      |:|:\  \     /:/ /\  \  
  _\:\ \:\  \   /:/ /:/  /    __|:|\:\  \   /:/ /::\  \ 
 /\ \:\ \:\__\ /:/_/:/__/___ /::::|_\:\__\ /:/_/:/\:\__\
 \:\ \:\/:/  / \:\/:::::/  / \:\~~\  \/__/ \:\/:/ /:/  /
  \:\ \::/  /   \::/~~/~~~~   \:\  \        \::/ /:/  / 
   \:\/:/  /     \:\~~\        \:\  \        \/_/:/  /  
    \::/  /       \:\__\        \:\__\         /:/  /   
     \/__/         \/__/         \/__/         \/__/    
	 
	 # Copyright 2013 by wrcode
     # Feel free to modify the source
     # Don't sell without author permission	 
						
*/
?>

<? $mysql -> admin_check($_SESSION['admin']); ?>

<? if(isset($_POST['sidebar_name']) && empty($_GET['edit'])) $sidebar ->SidebarNew($_POST['sidebar_name'],$_POST['sidebar_desc']); ?>

<? if(!empty($_POST['item_edition'])) $sidebar ->item_edit_save($_POST['item_id'],$_POST['item_name'],$_POST['item_link'],$_POST['item_strona'],$_POST['item_content']);?>

<? if(!empty($_GET['delete']) && empty($_GET['element_id'] )) $sidebar ->SideBarDelete($_GET['delete']); ?>

<? if($_GET['edit'] && $_POST['sidebar_name']) $sidebar ->SideBarEditSave($_GET['edit'],$_POST['sidebar_name'],$_POST['sidebar_desc']);  ?>

<? if(!empty($_GET['sidebar_id']) && $_GET['item_delete']) $sidebar ->item_delete($_GET['item_delete'],$_GET['item_pos']);  ?>

<? if($_GET['element_id'] && $_GET['move']) { $sidebar ->item_move($_GET['move'],$_GET['element_id'],$_GET['menu_id']); }  ?>

<? if($_POST['item_name'] && empty($_POST['item_id'])) { $sidebar ->item_new($_POST['item_name'],$_POST['item_link'],$_GET['sidebar_id'],$_POST['item_strona'],$_POST['item_content']); }  ?>


<div id="header">
    <img src="images/icons/settings.png" />
    <div class="header-content">
        <span class="header-title">Ustawienia</span><br />
        <span class="header-tagline">Zarzadzanie strona</span>
    </div>
</div>
<div id="tabs">

   <a href="index.php?site=main">Strona główna</a>
   <a href="index.php?site=wpisy">Wpisy</a>
   <a href="index.php?site=podstrony">Podstrony</a>
   <a href="index.php?site=galeria">Galeria</a>
   <span>Ustawienia</span>
   <a href="index.php?site=user">Uzytkownicy</a>


   
</div>
<div class="container cf">
    <div class="side-left">
	
        <div class="side-left-title">Ogolne</div>
        <div class="side-left-content">
            <a href="index.php?site=ustawienia">Ustawienia ogolne</a><br />
            <a href="index.php?site=ustawienia&type=admin">Ustawienia admina</a><br />
			
        </div>
		
		<div class="side-left-title">Wpisy</div>
        <div class="side-left-content">
            <a href="index.php?site=ustawienia&type=wpisy">Ustawienia wpisow</a><br />
        </div>
		
		<div class="side-left-title">Szablon</div>
		
		 <div class="side-left-content">
             <a href="index.php?site=szablon">Panel boczny</a><br />
			 <a href="index.php?site=szablon&action=add">Stworz panel</a><br />
        </div>
		
		<div class="side-left-title">Menu</div>
        <div class="side-left-content">
            <a href="index.php?site=menu">Menu lista</a><br />
			<a href="index.php?site=menu&action=add">Stworz menu</a><br />
        </div>
		
		
		
    </div>
	
    <div class="side-right">
        <div class="side-right-title">Zarzadzanie panelem<?if(!empty($_GET['sidebar_id'])) { ?> <span style="float:right;padding-right:10px"><a href="index.php?site=szablon&sidebar_id=<?=$_GET['sidebar_id'];?>&action=add_element">Dodaj element</a></span> <? } ?></div>

        <div class="side-right-content">
		
		<? if(!isset($_GET['action']) && empty($_GET['edit']) and !empty($_GET['sidebar_id'])) { ?>
		
		
		    <table class="pages-list" width="100%" cellpadding="10px">
                <tr class="pages-list-title">
                    <th class="pages-list-id">ID</th>
					<th class="pages-list-id">Pozycja</th>
                    <th class="pages-list-name">Nazwa elementu</th>
                    <th class="pages-list-pos">Nazwa menu</th>
					<th class="pages-list-medium">Item type</th>
					<th class="pages-list-pos">Pozycja elementu </th>
					<th class="pages-list-medium">Data utworzenia </th>
                    <th class="pages-list-act">Dzialania</th>
                </tr>
				
				
				<? $elements =  $sidebar -> SideBarElements($_GET['sidebar_id']); ?>
				
				<? for($i=0;$i<count( $elements );$i++) { ?>
				
                <tr class="pages-list-item">
				
				
                    <td><?=$elements[$i]->item_id; ?></td>
					
					<td><?=$elements[$i]->item_sidebar_pos; ?></td>
					
                    <td>
                       <?=$elements[$i]->item_name; ?>
                    </td>
					
					<td style="text-align:center">
					<?php echo $sidebar->GetSideBarName($elements[$i]->item_sidebar_id); ?>
					</td>
					
					<td style="text-align:center">
					<?=$elements[$i]->item_type; ?>
					</td>
					
					<td style="text-align:center">
					<a class="podpowiedz" title="Kliknij aby ustawic element nizej" href="index.php?site=szablon&sidebar_id=<?=$elements[$i]->item_sidebar_id; ?>&element_id=<?=$elements[$i]->item_id; ?>&move=movedown"><img src="images/icons/ic_down.png"></a>
					<a class="podpowiedz" title="Kliknij aby ustawic element wyzej"  href="index.php?site=szablon&sidebar_id=<?=$elements[$i]->item_sidebar_id; ?>&element_id=<?=$elements[$i]->item_id; ?>&move=moveup"><img src="images/icons/ic_up.png"></a>
					</td>
					
					<td style="text-align:center">
					<?=$elements[$i]->item_date; ?>
					</td>

					
                <td style="text-align:center">
					
					    <a class="podpowiedz" title="Usun element" href="index.php?site=szablon&sidebar_id=<?=$elements[$i]->item_sidebar_id; ?>&item_delete=<?=$elements[$i]->item_id; ?>&item_pos=<?=$elements[$i]->item_menu_pos; ?>" onclick="if(confirm('Czy napewno chcesz usunac element?')) {} else return false;">
						<img src="images/icons/ic_delete.png" />
						</a>
					
                        <a class="podpowiedz" title="Edytuj element" href="index.php?site=szablon&sidebar_id=<?=$elements[$i]->item_sidebar_id; ?>&item_edit=<?=$elements[$i]->item_id; ?>&action=edit" >
						<img src="images/icons/ic_edit.png" />
						</a>
						
						
						
                </td>
				
                </tr>
				
				<? } ?>	
				
		    </table>
		
	
		
		
		<? } elseif(!isset($_GET['action']) && empty($_GET['edit'])) { ?>
            <table class="pages-list" width="100%" cellpadding="10px">
                <tr class="pages-list-title">
                    <th class="pages-list-id">ID</th>
                    <th class="pages-list-name">Nazwa panelu</th>
					<th class="pages-list-pos">Opis panelu</th>
                    <th class="pages-list-pos">Ilosc elementow</th>
					<th class="pages-list-pos">Data utworzenia </th>
                    <th class="pages-list-act">Dzialania</th>
                </tr>
				
				
				<? $sidebars =  $sidebar -> SideBars(); ?>
				
				<? for($i=0;$i<count($sidebars);$i++) { ?>
				
                <tr class="pages-list-item">
				
                    <td><?=$sidebars[$i]->sidebar_id; ?></td>
					
                    <td>
                        <a href="index.php?site=szablon&sidebar_id=<?=$sidebars[$i]->sidebar_id; ?>" class="podpowiedz" title="<?=$sidebars[$i]->sidebar_desc;   ?>" ><?=$sidebars[$i]->sidebar_name; ?></a>
                    </td>
					
					<td style="text-align:center">
					<?=$sidebars[$i]->sidebar_desc; ?>
					</td>
					
					<td style="text-align:center">
					<?php echo $sidebar->get_sidebar_count($sidebars[$i]->sidebar_id); ?>
					</td>
					
					<td style="text-align:center">
					<?=$sidebars[$i]->sidebar_date; ?>
					</td>

					
                <td style="text-align:center">
					
					    <a href="index.php?site=szablon&delete=<?=$sidebars[$i]->sidebar_id; ?>" onclick="if(confirm('Czy napewno chcesz usunac menu?')) {} else return false;">
						<img src="images/icons/ic_delete.png" />
						</a>
					
                        <a href="index.php?site=szablon&edit=<?=$sidebars[$i]->sidebar_id; ?>" >
						<img src="images/icons/ic_edit.png" />
						</a>
						
						
						
                </td>
                </tr>
				
				
		   
			
				<? } ?>
				
			</table>
                         
							
<? } elseif($_GET['action']=='add' || isset($_GET['edit'] )) 
	{ 
	if(isset($_GET['edit'])) $edit = '&edit='.$_GET['edit'].'';
	
    $edited = $sidebar -> SideBarEdit($_GET['edit']); 
							
?> 
							
		<form action="index.php?site=szablon<?=$edit; ?>" method="post">

            <table class="pages-create" width="100%" cellpadding="10px">
                <input type="hidden" name="type" value="static">
                <tbody>
                <tr>
                    <td>Nazwa</td>
                    <td><input type="text" x-webkit-speech="x-webkit-speech" value="<?=$edited->sidebar_name; ?>" name="sidebar_name"></td>
					
					<td>Opis</td>
                    <td><input type="text" x-webkit-speech="x-webkit-speech" value="<?=$edited->sidebar_desc; ?>" name="sidebar_desc"></td>
					

					
                   <td colspan="2"><button type="submit" class="button submit image-right ic-right-arrow">Stworz</button></td>
   
                </tr>
				
				</table>
				    
        
            
            </tbody>
            </form>
							
							
			<? } elseif(!empty($_GET['item_edit']) && $_GET['action'] == 'edit' || $_GET['action'] =='add_element' )  { ?>
							
			<? $edited = $sidebar -> EditItem($_GET['item_edit']); ?>				
							
			<form action="index.php?site=szablon&sidebar_id=<?=$_GET['sidebar_id'];?>" method="post">

            <table class="pages-create" width="100%" cellpadding="10px">
			
                <input type="hidden" name="item_edition" value="1">
				
				<input type="hidden" name="item_id" value="<?=$edited[0]->item_id; ?>">
		
                <tr>
				
                    <td>Nazwa</td>
                    <td><input x-webkit-speech="x-webkit-speech" type="text" value="<?=$edited[0]->item_name; ?>" name="item_name"></td>
					
					<td>Typ</td>
                    <td><input x-webkit-speech="x-webkit-speech" id="item_link" type="text" value="<?=$edited[0]->item_link; ?>" name="item_link"></td>
					
   
                </tr>
			
				
				</table>
		    <table class="pages-create" width="100%" cellpadding="10px">
                <tr>
                    <td>
					
					<textarea class="ckeditor"  id="edytor" name="item_content"><?=$edited[0]->item_content; ?></textarea>
					
					</td>
					
					</tr>
					
					<tr>
					
					 <td colspan="2"><button type="submit" class="button submit image-right ic-right-arrow">Send</button></td>
					
					</tr>
            
       
			</table>
        
            
     
            </form>
							
							
							
							
							<? } ?>
        </div>
    </div>
</div>



<? include "./footer.php" ?>