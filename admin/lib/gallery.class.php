<? 

class gallery extends mysql {

    public function __construct() {
            $this->db = new mysql;
			$this->db->connection();
	}
	
	public function gallerys() {
		$loop = $this -> db -> the_loop('gallery');
		return $loop;
	}

	public function gallery_new($gal_name,$gal_desc) {
		$gal_name =   $this -> db -> clean($gal_name);
		$gal_desc =  $this -> db -> clean($gal_desc);
		$date = date("d-m-y");
		$this -> db -> safe_query("INSERT INTO ".PREFIX."gallery (gal_name,gal_desc,gal_date) VALUES ('$gal_name','$gal_desc','$date')") or die(mysql_error());
		display_notification('Gratulacje','Udalo ci sie dodac galerie.');
	}

	public function gallery_delete($gal_id) {
		$gal_id =  $this -> db -> clean($gal_id);
			$this -> db -> safe_query("DELETE FROM ".PREFIX."gallery where gal_id =  '$gal_id' ");
		display_notification('Gratulacje','Udalo ci sie usunac galerie.');
	}

	public function gallery_edit($gal_id) {
		$gal_id = $this -> db -> clean($gal_id);
			if(is_numeric($gal_id)) {
				$query = $this -> db -> safe_query("SELECT * FROM ".PREFIX."gallery where gal_id='$gal_id'");
				$gallery = mysql_fetch_object($query);
				return $gallery;
			}
	}

	public function gallery_edit_save($gal_id,$gal_name,$gal_desc) {
		$gal_name = $this -> db -> clean($gal_name);
		$gal_desc = $this -> db -> clean($gal_desc);
			if(is_numeric($gal_id)) {
				$this -> db -> safe_query("UPDATE ".PREFIX."gallery set gal_name = '$gal_name' , gal_desc = '$gal_desc' where gal_id = '$gal_id' ") or die(mysql_error());
			}
		display_notification('Gratulacje','Udalo ci sie edytowac galerie,zmiany sa natychmiastowe.');
	}

	public function gallery_images($gallery_id) {
			if(!empty($gallery_id)) 
				$dbres = $this -> db -> safe_query("SELECT * FROM ".PREFIX."gallery_images where gallery_id = '$gallery_id'");
			else 	
				$dbres = $this -> db -> safe_query("SELECT * FROM ".PREFIX."gallery_images");
				$rows = mysql_num_rows($dbres);
					for($x=0;$x<$rows;$x++) {
						$result = mysql_fetch_object($dbres);
						$row[$x]=$result;
					}		 
				return $row;
	}

	public function gallery_image_add($gallery_id,$image_name,$image_filename,$image_desc,$image_slider) {
		$gallery_id =  $this -> db -> clean($gallery_id);
		$image_name = $this -> db -> clean($image_name);
		$image_desc = $this -> db -> clean($image_desc);
		$image_file_name = $image_filename['name'];
		$target_path = "./data/uploads/";
		$target_path = $target_path . basename( $image_filename['name']); 
			if(move_uploaded_file($image_filename['tmp_name'], $target_path)) {
			} 
				$this -> db -> safe_query("INSERT INTO ".PREFIX."gallery_images (gallery_id,image_name,image_filename,image_desc.image_slider) VALUES ('$gallery_id','$image_name','$image_file_name','$image_desc','$image_slider') ") or die(mysql_error());
				$this -> db -> safe_query("UPDATE ".PREFIX."gallery SET gal_count = gal_count+1 where gal_id = '$gallery_id' ");
			display_notification('Gratulacje','Udalo ci sie dodac obraz.');
	}

	public function gallery_image_delete($image_id) {
		$image_id = $this -> db -> clean($image_id);
		$result = $this -> db -> safe_query("SELECT * FROM ".PREFIX."gallery_images where image_id = '$image_id'");
		$obj = mysql_fetch_object($result);
		unlink("./data/uploads/".$obj->image_filename);
			$this -> db -> safe_query("DELETE FROM ".PREFIX."gallery_images where image_id = '$image_id'");
			$this -> db -> safe_query("UPDATE ".PREFIX."gallery SET gal_count = gal_count-1 where gal_id = '".$obj->gallery_id."' ");
		display_notification('Gratulacje','Udalo ci sie usunac obraz.');
	}

	public function gallery_image_edit($image_id)  {
		$image_id = $this -> db -> clean($image_id);
			if(is_numeric($image_id)) {
				$query = $this -> db -> safe_query("SELECT * FROM ".PREFIX."gallery_images where image_id='$image_id'");
				$image = mysql_fetch_object($query);
			return $image;
			}
	}

	public function gallery_image_edit_save($image_id,$image_name,$image_desc,$image_file,$gallery_id,$image_slider) {
		$image_id = $this -> db -> clean($image_id);
		$image_name = $this -> db -> clean($image_name);
		$image_desc = $this -> db -> clean($image_desc);
		$gallery_id =  $this -> db -> clean($gallery_id);
		$image_slider =  $this -> db -> clean($image_slider);
			if(is_numeric($image_id)) {
				$this -> db -> safe_query("UPDATE ".PREFIX."gallery_images SET gallery_id = '$gallery_id',image_name = '$image_name',image_filename = '$image_file' ,image_desc = '$image_desc' ,image_slider = '$image_slider' where image_id = '$image_id' ");
			}
		display_notification('Gratulacje','Udalo ci sie edytowac obraz.');
	}

	public function gallery_set_thumbnail($image_id,$gallery_id) {
		$image_id =  $this -> db -> clean($image_id);
		$gallery_id =  $this -> db -> clean($gallery_id);
		$result =  $this -> db -> safe_query("SELECT * FROM ".PREFIX."gallery_images where image_id = '$image_id'")  or die(mysql_error());
		$obj = mysql_fetch_object($result);
			$this -> db -> safe_query("UPDATE ".PREFIX."gallery set gal_thumbnail = 'data/uploads/".$obj -> image_filename."' where gal_id = '$gallery_id'") or die(mysql_error()); 
			display_notification('Gratulacje','Obrazek zostal ustawiony jako okladka galeri.');
		}

	}

?>