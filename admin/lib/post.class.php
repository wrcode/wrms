<? 

class post extends mysql {

    public function __construct() {
            $this->db = new mysql;
			$this->db->connection();
	}

	public function posts() {
	    $loop = $this -> db -> the_loop('posts');
		return $loop;
	}

	public function post_category($id) {
		$id =  $this -> db -> clean($id);
		$result = $this -> db -> safe_query("SELECT * FROM ".PREFIX."categorys where cat_id = '$id' ");
		$obj =  mysql_fetch_object($result);
	    return $obj->cat_name;
	}

	public function post_new($post_name,$post_content,$post_description,$post_visible,$post_category) {
		$post_name =  $this -> db -> clean($post_name);
		$post_content = $post_content;
		$post_description =  $this -> db -> clean($post_description);
		$post_visible =  $this -> db -> clean($post_visible);
		$post_category =  $this -> db -> clean($post_category);
		$post_date = date("d-m-y");
		$this -> db -> safe_query("INSERT INTO ".PREFIX."posts (post_id,post_name,post_content,post_description,post_visible,post_category,post_date) VALUES ('$post_id','$post_name','$post_content','$post_description','$post_visible','$post_category','$post_date') ") or die(mysql_error());
		display_notification('Gratulacje','Udalo ci sie utworzyc wpis bedzie on teraz widoczny na liscie najnowszych wpisow.');
	}

	public function post_edit($post_id) {
		$post_id =  $this -> db -> clean($post_id);
			if(is_numeric($post_id)) {
				$query = $this -> db -> safe_query("SELECT * FROM ".PREFIX."posts where post_id='$post_id'");
				$posts = mysql_fetch_object($query);
			return $posts;
		   }
	}

	public function post_edit_save($post_id,$post_name,$post_content,$post_description,$post_visible,$post_category) { 
		$id =  $this -> db -> clean($id);
		$post_name =  $this -> db -> clean($post_name);
		$post_content =  $post_content;
		$post_description =  $this -> db -> clean($post_description);
		$post_visible =  $this -> db -> clean($post_visible);
		$post_category =  $this -> db -> clean($post_category);
		$post_edit_date = date("d-m-y");
			$this -> db -> safe_query("UPDATE ".PREFIX."posts SET post_name = '$post_name' , post_content = '$post_content' , post_description = '$post_description' , post_visible = '$post_visible' ,post_category = '$post_category' ,post_last_edit = '$post_edit_date' where post_id = '$post_id' ") or die(mysql_error());
			display_notification('Gratulacje','Udalo ci sie edytowac post zmiany sa widoczne natychmiast.');
	}

	public function post_delete($post_id) {
		$post_id =  $this -> db -> clean($post_id);
			if(is_numeric($post_id)) {
				$this -> db -> safe_query("DELETE  FROM ".PREFIX."posts where post_id = '$post_id'") or die(mysql_error());
				display_notification('Gratulacje','Udalo ci sie usunac wpis.');
		   }
	}

	public function post_visible($number){
		$number =  $this -> db -> clean($number);
		if($number == '1') { return 'Tak'; } else { return 'Nie'; }
	}

	public function comments() {
		$dbres = $this -> db -> safe_query("SELECT * FROM ".PREFIX."posts_comments");
		$rows = mysql_num_rows($dbres);
		for($x=0;$x<$rows;$x++){
			$result = mysql_fetch_object($dbres);
			$row[$x]=$result;
		} 
	    return $row;
	}
	
	public function comment_delete($com_id) {
		$com_id =  $this -> db -> clean($com_id);
			if(is_numeric($com_id)) {
				$this -> db -> safe_query("DELETE  FROM ".PREFIX."posts_comments where cm_id = '$com_id'") or die(mysql_error());
				display_notification('Gratulacje','Udalo ci sie usunac komentarz.');
			}
	}

	public function comment_edit($cm_id) {
		$cm_id =  $this -> db -> clean($cm_id);
			if(is_numeric($cm_id)) {
				$query = $this -> db -> safe_query("SELECT * FROM ".PREFIX."posts_comments where cm_id='$cm_id'");
				$date = date("d-m-y");
				$this -> db -> safe_query("UPDATE ".PREFIX."posts_comments SET cm_date_edit = '$date' where cm_id = '$cm_id'");
			    $comments = mysql_fetch_object($query);
			   return $comments;
			}
	}

	public function comment_edit_save($cm_id,$cm_author,$cm_content,$cm_author_email,$cm_banned){
		$cm_id      = $this -> db -> clean($cm_id);
		$cm_author  = $this -> db -> clean($cm_author);
		$cm_content = $this -> db -> clean($cm_content);
		$cm_banned  = $this -> db -> clean($cm_banned);
			if(is_numeric($cm_id )) {
				$this -> db -> safe_query("UPDATE ".PREFIX."posts_comments SET cm_author = '$cm_author' ,cm_content = '$cm_content',cm_author_email = '$cm_author_email',cm_banned = '$cm_banned' where cm_id = '$cm_id' ") or die(mysql_error());
			}
		display_notification('Gratulacje','Udalo ci sie edytowac komentarz.');
	}

}

?>