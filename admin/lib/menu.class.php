<? 

    class menu extends mysql {


        public function __construct() 
        {
            $this->db = new mysql;
			$this->db->connection();
	    }


    public function menus() {
		$loop = $this -> db -> the_loop('menus');
        return $loop;
	}


    public function menu_edit($menu_id) {
        $menu_id = $this -> db -> clean($menu_id);
            if(is_numeric($menu_id)) {
                $query = $this -> db -> safe_query("SELECT * FROM ".PREFIX."menus where menu_id='$menu_id'");
                $menu = mysql_fetch_object($query);
                return $menu;
            }
    }
	
	 public function menu_edit_save($menu_id,$menu_name,$menu_desc) {
        $menu_id = $this -> db -> clean($menu_id);
            if(is_numeric($menu_id)) {
                $this -> db -> safe_query("UPDATE ".PREFIX."menus SET menu_name = '$menu_name' , menu_desc = '$menu_desc' where menu_id = '$menu_id'");
				display_notification('Gratulacje','Udalo ci sie edytowac menu.');
            }
    }
	
	public function menu_new($menu_name,$menu_desc) {
        $menu_name = $this -> db -> clean($menu_name);
        $menu_desc = $this -> db -> clean($menu_desc);
        $menu_date = date("d-m-y");
        $this -> db -> safe_query("INSERT INTO ".PREFIX."menus (menu_id,menu_name,menu_desc,menu_date) VALUES ('','$menu_name','$menu_desc','$menu_date') ") or die(mysql_error());
        display_notification('Gratulacje','Udalo ci sie utworzyc nowe menu.');
    }
	
	public function get_menu_count($menu_id) {
	    $menu_id = $this -> db -> clean($menu_id);
            if(is_numeric($menu_id)) {
			    $result = $this -> db -> safe_query("SELECT * FROM ".PREFIX."menus_items where item_menu_id = '$menu_id'");
			    $num_rows = mysql_num_rows($result);
				return $num_rows;
			}
	}
	
	public function get_menu_count_item($item_id) {
	    $item_id = $this -> db -> clean($item_id);
            if(is_numeric($item_id)) {
			    $result = $this -> db -> safe_query("SELECT * FROM ".PREFIX."menus_items where item_id = '$item_id'");
			    $num_rows = mysql_fetch_object($result);
				return $num_rows->item_menu_id;
			}
	}
	
	public function get_menu_name($menu_id) {
	    if(is_numeric($menu_id)) {
		    $result = $this -> db -> safe_query("SELECT * FROM ".PREFIX."menus where menu_id = '$menu_id'");
			$obj =  mysql_fetch_object($result);
		    return $obj->menu_name;
		}
	}
	
	
	public function menu_elements($menu_id) {
	    $menu_id = $this -> db -> clean($menu_id);
	    $dbres = $this -> db -> safe_query("SELECT * FROM ".PREFIX."menus_items  where item_menu_id = '$menu_id' ORDER BY item_menu_pos ASC ");
            $rows = mysql_num_rows($dbres);
                for($x=0;$x<$rows;$x++) {
                    $result = mysql_fetch_object($dbres);
                    $row[$x]=$result;
                }
                return $row;
	}
	
	public function GetItemIdbyPos($item_pos) {
	    $item_pos = $this -> db -> clean($item_pos);
	    $dbres = $this -> db -> safe_query("SELECT * FROM ".PREFIX."menus_items  where item_menu_pos = '$item_pos'");
            $rows = mysql_num_rows($dbres);
                for($x=0;$x<$rows;$x++) {
                    $result = mysql_fetch_object($dbres);
                    $row[$x]=$result;
                }
                return $row;
	}
	
	public function item_move($move,$item_id,$menu_id) { 
		$item_id =  $this ->  db -> clean($item_id);
		$menu_count = $this -> get_menu_count($this->get_menu_count_item($item_id));
	    if(is_numeric($item_id)) {
		    $result =  $this ->  db -> query("SELECT * FROM ".PREFIX."menus_items where item_id = '$item_id' ",'select') or die(mysql_error());
		    if($result[0] -> item_menu_pos == 0 && $move == 'moveup' ) {
		        display_notification('Blad','Przepraszamy ten element jest najwyzej.');
		    } elseif ( $result[0] -> item_menu_pos > 0 && $move == 'moveup' ) {
		        $prev_pos = $result[0] -> item_menu_pos;
				$new_pos = $result[0] -> item_menu_pos - 1;
				$item =  $this ->  db -> query("SELECT * FROM ".PREFIX."menus_items where item_menu_pos = '$new_pos' and item_menu_id = '$menu_id'  ",'select') or die(mysql_error());		
				$prev_id = $item[0] -> item_id;				
				$menu_id = $item[0] -> item_menu_id;				
				$next_pos = $new_pos+1;				
				$item_prev_pos =  $this -> GetItemIdbyPos($next_pos);				
				$item_prev_pos = $item_prev_pos[0] -> item_id;				
				$this -> db -> query("UPDATE ".PREFIX."menus_items SET item_menu_pos = '$new_pos'  where item_menu_pos = '$prev_pos' and item_menu_id = '$menu_id' and item_id = '$item_id'  ");
				$this -> db -> query("UPDATE ".PREFIX."menus_items SET item_menu_pos = '$next_pos'  where item_id = '$prev_id' and item_menu_id = '$menu_id' ");
				display_notification('Gratulacje','Zmiena polozenia elementu przebiegla pomyslnie   ');		   
		   } elseif(		   
		    $move == 'movedown' && $result[0] -> item_menu_pos + 2  > $menu_count ) { display_notification('Blad','Przepraszamy ten element jest najnizej.'); 		   
		   } elseif($move == 'movedown' && $result[0] -> item_menu_pos > $prev_pos ) {		   
		        $menu_count = $this -> get_menu_count($menu_id);		   
		        $prev_pos = $result[0] -> item_menu_pos;				
				$new_pos = $result[0] -> item_menu_pos + 1;				
				$item =  $this ->  db -> query("SELECT * FROM ".PREFIX."menus_items where item_menu_pos = '$new_pos' and item_menu_id = '$menu_id' ",'select');				
				$this -> db -> query("UPDATE ".PREFIX."menus_items SET item_menu_pos = '$new_pos'  where item_menu_pos = '$prev_pos' and item_menu_id = '$menu_id' and item_id = '$item_id'");				
                $prev_id = $item[0] -> item_id;							
				$this -> db -> query("UPDATE ".PREFIX."menus_items SET item_menu_pos = '$prev_pos'  where item_id = '$prev_id' and item_menu_id = '$menu_id' ");				
		        display_notification('Gratulacje','Zmiena polozenia elementu przebiegla pomyslnie ');		   		   
		   } 
	    }
	}
	
	
	public function item_new($item_name,$item_link,$item_menu_id,$item_strona) {
	    $item_name = $this -> db -> clean($item_name);
		if(empty($item_link)) $item_link = $item_strona;
		$item_menu_id = $this -> db -> clean($item_menu_id);
	        if(is_numeric($item_menu_id)) {
	           $last_pos =  $this -> db -> query("SELECT * FROM ".PREFIX."menus_items where item_menu_id = '$item_menu_id' ",'select');
			   $position =  count($last_pos);
			   $date = date('y-m-d h-m-s');
			   $this -> db -> query("INSERT INTO ".PREFIX."menus_items (item_name,item_link,item_menu_pos,item_menu_id,item_date) VALUES ('$item_name','$item_link','$position','$item_menu_id','$date')",'insert');
	            display_notification('Gratulacje','Dodanie elementu przebieglo pomyslnie');
	        }
	}
	
	public function item_delete($item_id,$item_pos) {
        $item_id =  $this -> db -> clean($item_id);
            if(is_numeric($item_id)) {
			   $deleted = $this -> db ->query("SELECT * FROM ".PREFIX."menus_items where item_menu_id = '$item_id' ",'select');
               $this -> db -> query("DELETE  FROM ".PREFIX."menus_items where item_id = '$item_id'");
			   $position = $this -> db ->query("SELECT * FROM ".PREFIX."menus_items",'select');
			   for($i=$item_pos;$i<count($position)+1;$i++) {
			      $this -> db -> safe_query("UPDATE ".PREFIX."menus_items SET item_menu_pos = item_menu_pos -1  where item_menu_pos = '$i' ");
			   }
			    display_notification('Gratulacje','Udalo ci sie usunac element.');
            }
	}
	
	public function menu_delete($menu_id) {
	    $menu_id =  $this -> db -> clean($menu_id);
            if(is_numeric($menu_id)) {
			    $this -> db -> query("DELETE  FROM ".PREFIX."menus where menu_id = '$menu_id'  ",'delete');
				$this -> db -> query("DELETE  FROM ".PREFIX."menus_items where item_menu_id = '$menu_id'",'delete');
				display_notification('Gratulacje','Menu zostalo usuniete.');
			}
	}
	
	public function edit_item($item_id){ 
	    $item_id =  $this ->  db -> clean($item_id);
		if(is_numeric($item_id)) {
		    $item = $this -> db -> query("SELECT * FROM ".PREFIX."menus_items WHERE item_id = '$item_id'",'select');
			return $item;
		}
	}
	
	public function item_edit_save($item_id,$item_name,$item_link,$item_strona) {
	if(empty($item_link)) $item_link = $item_strona;
	$item_id = $this -> db -> clean($item_id);
	    if(is_numeric($item_id)) {
	        $item_name = $this -> db -> clean($item_name);
			$item = $this -> db -> query("UPDATE ".PREFIX."menus_items SET item_name = '$item_name' , item_link = '$item_link' WHERE item_id = '$item_id' ");
			display_notification('Gratulacje','Element zostal pomyslnie edytowany.');
		}
	}
}	

?>