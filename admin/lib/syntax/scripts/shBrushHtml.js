/**
2	 * SyntaxHighlighter
3	 * http://alexgorbatchev.com/
4	 *
5	 * SyntaxHighlighter is donationware. If you are using it, please donate.
6	 * http://alexgorbatchev.com/wiki/SyntaxHighlighter:Donate
7	 *
8	 * @version
9	 * 2.1.364 (October 15 2009)
10	 *
11	 * @copyright
12	 * Copyright (C) 2004-2009 Alex Gorbatchev.
13	 *
14	 * @license
15	 * This file is part of SyntaxHighlighter.
16	 *
17	 * SyntaxHighlighter is free software: you can redistribute it and/or modify
18	 * it under the terms of the GNU Lesser General Public License as published by
19	 * the Free Software Foundation, either version 3 of the License, or
20	 * (at your option) any later version.
21	 *
22	 * SyntaxHighlighter is distributed in the hope that it will be useful,
23	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
24	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
25	 * GNU General Public License for more details.
26	 *
27	 * You should have received a copy of the GNU General Public License
28	 * along with SyntaxHighlighter.  If not, see <http://www.gnu.org/copyleft/lesser.html>.
29	 */
	SyntaxHighlighter.brushes.Xml = function()
	{
	        function process(match, regexInfo)
	        {
	                var constructor = SyntaxHighlighter.Match,
	                        code = match[0],
	                        tag = new XRegExp('(&lt;|<)[\\s\\/\\?]*(?<name>[:\\w-\\.]+)', 'xg').exec(code),
	                        result = []
	                        ;
	               
	                if (match.attributes != null) 
	                {
	                        var attributes,
	                                regex = new XRegExp('(?<name> [\\w:\\-\\.]+)' +
	                                                                        '\\s*=\\s*' +
	                                                                        '(?<value> ".*?"|\'.*?\'|\\w+)',
	                                                                        'xg');
	
	                        while ((attributes = regex.exec(code)) != null) 
	                        {
	                                result.push(new constructor(attributes.name, match.index + attributes.index, 'color1'));
	                                result.push(new constructor(attributes.value, match.index + attributes.index + attributes[0].indexOf(attributes.value), 'string'));
	                        }
	                }
	
	                if (tag != null)
	                        result.push(
	                                new constructor(tag.name, match.index + tag[0].indexOf(tag.name), 'keyword')
	                        );
	
	                return result;
	        }
	       
	        this.regexList = [
	                { regex: new XRegExp('(\\&lt;|<)\\!\\[[\\w\\s]*?\\[(.|\\s)*?\\]\\](\\&gt;|>)', 'gm'),                   css: 'color2' },        // <![ ... [ ... ]]>
	                { regex: SyntaxHighlighter.regexLib.xmlComments,                                                                                                css: 'comments' },      // <!-- ... -->
	                { regex: new XRegExp('(&lt;|<)[\\s\\/\\?]*(\\w+)(?<attributes>.*?)[\\s\\/\\?]*(&gt;|>)', 'sg'), func: process }
	        ];
	};
	
	SyntaxHighlighter.brushes.Xml.prototype = new SyntaxHighlighter.Highlighter();
	SyntaxHighlighter.brushes.Xml.aliases   = ['xml', 'xhtml', 'xslt', 'html'];
