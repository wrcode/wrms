<? include "lib/global.php";
/*                                                                                                                                                                                                                  
      ___           ___           ___           ___     
     /\  \         /\  \         /\  \         /\__\    
    _\:\  \       /::\  \       |::\  \       /:/ _/_   
   /\ \:\  \     /:/\:\__\      |:|:\  \     /:/ /\  \  
  _\:\ \:\  \   /:/ /:/  /    __|:|\:\  \   /:/ /::\  \ 
 /\ \:\ \:\__\ /:/_/:/__/___ /::::|_\:\__\ /:/_/:/\:\__\
 \:\ \:\/:/  / \:\/:::::/  / \:\~~\  \/__/ \:\/:/ /:/  /
  \:\ \::/  /   \::/~~/~~~~   \:\  \        \::/ /:/  / 
   \:\/:/  /     \:\~~\        \:\  \        \/_/:/  /  
    \::/  /       \:\__\        \:\__\         /:/  /   
     \/__/         \/__/         \/__/         \/__/    
	 
	 # Copyright 2013 by wrcode
     # Feel free to modify the source
     # Don't sell without author permission	 
						
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>WRMS</title>

<link rel="stylesheet" href="html/css/bootstrap.csss" />

<link rel="stylesheet" href="lib/css/style.css" />
<link rel="stylesheet" type="text/css" href="lib/css/tipTip.css">
<link rel="stylesheet" type="text/css" href="lib/css/lightbox.css">
<link rel="stylesheet" type="text/css" href="lib/css/ui.notify.css" />

<link type="text/css" rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/redmond/jquery-ui.css" />

<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.0.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript" src="lib/js/jquery.tipTip.js"></script>
<script type="text/javascript" src="lib/js/lightbox.js"></script>
<script type="text/javascript" src="lib/js/Chart.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript" src="lib/js/jquery.notify.js"></script>
<script type="text/javascript" src="lib/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="lib/syntax/scripts/shCore.js"></script>
<script type="text/javascript" src="lib/syntax/scripts/shBrushJScript.js"></script>
<script type="text/javascript" src="lib/syntax/scripts/shBrushCss.js"></script>
<script type="text/javascript" src="lib/syntax/scripts/shBrushHtml.js"></script>
<link type="text/css" rel="stylesheet" href="lib/syntax/styles/shCoreDefault.css"/>
<script type="text/javascript">
SyntaxHighlighter.all();

</script>
<script type="text/javascript" >
   
   $(function(){
    
     $(".podpowiedz").tipTip();
});

</script>

</head>
<body>

<?php 
function display_notification($title,$content) {
print '<script type="text/javascript"> function create( template, vars, opts ){ return $container.notify("create", template, vars, opts); } $(function(){ $container = $("#notification").notify();create("default", { title:\''.$title.'\', text:\''.$content.'\'});});</script> ';
} 
?>



	<div id="notification" style="display:none">
		
		<div id="default">
			<h1>#{title}</h1>
			<p>#{text}</p>
		</div>
		
	</div>



    <div id="freeow" class="freeow freeow-top-right"></div>
	<div id="bar">
		<span class="buttons-general cf">
			<a href="#" class="button dark image-right ic-user">Zalogowany jako <?=$_SESSION['admin_name'];?></a>
			<a href="../index.php" class="button dark image-right ic-right-arrow">Wroc do strony</a>
			<a href="index.php?action=logout" class="button dark image-right ic-log-out">Wyloguj</a>
		</span>
	</div>

