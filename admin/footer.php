<?
/*                                                                                                                                                                                                                  
      ___           ___           ___           ___     
     /\  \         /\  \         /\  \         /\__\    
    _\:\  \       /::\  \       |::\  \       /:/ _/_   
   /\ \:\  \     /:/\:\__\      |:|:\  \     /:/ /\  \  
  _\:\ \:\  \   /:/ /:/  /    __|:|\:\  \   /:/ /::\  \ 
 /\ \:\ \:\__\ /:/_/:/__/___ /::::|_\:\__\ /:/_/:/\:\__\
 \:\ \:\/:/  / \:\/:::::/  / \:\~~\  \/__/ \:\/:/ /:/  /
  \:\ \::/  /   \::/~~/~~~~   \:\  \        \::/ /:/  / 
   \:\/:/  /     \:\~~\        \:\  \        \/_/:/  /  
    \::/  /       \:\__\        \:\__\         /:/  /   
     \/__/         \/__/         \/__/         \/__/    
	 
	 # Copyright 2013 by wrcode
     # Feel free to modify the source
     # Don't sell without author permission	 
						
*/
?>
<div id="footer">Proudly powered by WRMS 1.0.0</div>

<center style="font-size:11px;">

<a rel="license" href="http://creativecommons.org/licenses/by-nc/3.0/deed.pl">
<img alt="Licencja Creative Commons" style="border-width:0" src="http://i.creativecommons.org/l/by-nc/3.0/80x15.png" /></a>
<br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/InteractiveResource" property="dct:title" rel="dct:type">wrms</span> 
by <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.facebook.com/wRcode" property="cc:attributionName" rel="cc:attributionURL">wrcode</a> 
is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/3.0/deed.pl">Creative Commons Uznanie autorstwa-Użycie niekomercyjne
 3.0 Unported License</a>.<br />W oparciu o utwór dostępny pod adresem <a xmlns:dct="http://purl.org/dc/terms/" href="https://www.facebook.com/wRcode" rel="dct:source">
 https://www.facebook.com/wRcode</a>
 
 </center>

</body>
</html>
