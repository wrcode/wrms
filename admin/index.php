<?
/*                                                                                                                                                                                                                  
      ___           ___           ___           ___     
     /\  \         /\  \         /\  \         /\__\    
    _\:\  \       /::\  \       |::\  \       /:/ _/_   
   /\ \:\  \     /:/\:\__\      |:|:\  \     /:/ /\  \  
  _\:\ \:\  \   /:/ /:/  /    __|:|\:\  \   /:/ /::\  \ 
 /\ \:\ \:\__\ /:/_/:/__/___ /::::|_\:\__\ /:/_/:/\:\__\
 \:\ \:\/:/  / \:\/:::::/  / \:\~~\  \/__/ \:\/:/ /:/  /
  \:\ \::/  /   \::/~~/~~~~   \:\  \        \::/ /:/  / 
   \:\/:/  /     \:\~~\        \:\  \        \/_/:/  /  
    \::/  /       \:\__\        \:\__\         /:/  /   
     \/__/         \/__/         \/__/         \/__/    
	 
	 # Copyright 2013 by wrcode
     # Feel free to modify the source
     # Don't sell without author permission	 
						
*/
session_start();

if($_GET['action']=='logout') {

session_unset(); 
 
session_destroy(); 

header("Location: index.php");

exit();

}

if(empty($_SERVER['QUERY_STRING'])) { include "pages/home.php"; }

if(!empty($_GET['site'])) { 

switch ($_GET['site']) {

   case 'kategorie' : include "pages/category.php"; break;
   case 'wpisy'     : include "pages/post.php"; break;
   case 'podstrony' : include "pages/podstrony.php"; break;
   case 'galeria'   : include "pages/galeria.php"; break;
   case 'obraz'     : include "pages/obraz.php"; break;
   case 'komentarze': include "pages/komentarze.php"; break;
   case 'ustawienia': include "pages/ustawienia.php"; break;
   case 'wyglad'    : include "pages/wyglad.php"; break;
   case 'edytor'    : include "pages/edytor.php"; break;
   case 'user'      : include "pages/user.php"; break;
   case 'menu'      : include "pages/menu.php"; break;
   case 'szablon'   : include "pages/szablon.php"; break;
   case 'main'      : include "pages/home.php"; break;
   
   default : header("Location: login.html"); exit();
   

}

}




?>